#include <iostream>

#include "swap.h"
#include "MyStack.h"
#include "MyStack2.h"

using namespace std;

static void test_function_template() {
    //Тема. Шаблоны функций.
    //Создайте шаблон функции перестановки местами двух
    //значений - Swap(). Проверьте работоспособность созданного
    //шаблона с помощью приведенного ниже фрагмента кода.
    //Подсказка 1: объявление шаблона корректнее поместить в .h-файл.

    int iX = 1, iY = -1;
    cout << "* Test function template *" << endl;
    cout << "Original: iX = " << iX << " iY = " << iY << endl;
    Swap(iX, iY);
    cout << "Swapped:  iX = " << iX << " iY = " << iY << endl;

    double dX = 0.5, dY = -5.5;
    cout << "Original: dX = " << dX << " dY = " << dY << endl;
    Swap(dX, dY);
    cout << "Swapped:  dX = " << dX << " dY = " << dY << endl;

    //Подсказка 2: подумайте, что нужно реализовать, для того,
    //			чтобы следующий вызов работал с объектами MyString
    //			не только корректно, но и эффективно
    //	MyString str1("One"), str2("Two");

    string str1("One"), str2("Two");
    cout << "Original: str1 = " << str1 << " str2 = " << str2 << endl;
    Swap(str1, str2);
    cout << "Swapped:  str1 = " << str1 << " str2 = " << str2 << endl;
    cout << endl;
}

static void test_class_mystack() {

    //Тема. Шаблоны классов.
    //Задание 1.
    //Создайте шаблон класса MyStack для хранения элементов любого типа T.
    //Подсказка: 1.элементы нужно где-то хранить - простым и удобным средством
    //			для хранения последовательно расположенных в памяти элементов
    //			является массив,
    //			2.для задания максимального размера стека может быть использован
    //			параметр-константа шаблона
    //			3.обязательными операциями со стеком являются "push" и "pop". Для
    //			того, чтобы гарантировать корректное выполнение этих операций
    //			хорошо было бы генерировать исключение в аварийной ситуации
    //			4. дополнительно реализуйте operator[] таким образом, чтобы пользователь
    //			мог читать/изменять значения только тех элементов, которые он формировал

    cout << "* Test class MyStack template *" << endl;
    cout << "Create MyStack<int> with default parameters" << endl;
    MyStack<> iStack_default;
    cout << "iStack_default size = " << iStack_default.size() << endl;

    cout << "Create MyStack with size 5" << endl;
    MyStack<int, 5> iStack;
    cout << "iStack size = " << iStack.size() << "  iStack current size = " << iStack.count() << endl;

    size_t last_index = 0;

    try {
        // push/pop 4 objects
        for (int i = 0; i < iStack.size() - 1; i++) {
            iStack.push(i*2);
            cout << "Pushed object #" << i << " iStack current size = " << iStack.count() << " value = " << iStack.operator[](i) << endl;
        }

        const int pop_value = iStack.pop();
        cout << "Pop last object." << " iStack current size = " << iStack.count() << endl;

        // get current index
        // get [] correct/incorrect objects
        last_index = iStack.count() - 1;
        cout << "Get object #" << last_index << ": { " << iStack.operator[](last_index) << " }" << endl;
        last_index++;
        cout << "Get object #" << last_index << ": { " << iStack.operator[](last_index) << " }" << endl;
    }
    catch (MyStackIndexError &err) {
        cout << "\nCannot get object #" << err.index << " because size is " << err.size << endl;
    }
    catch (out_of_range eRange) {
        cout << "\nGet object #" << last_index << " failed with message: " << eRange.what() << endl;
    }
    catch (...) {
        cout << "\nGet object #" << last_index << " failed!" << endl;
    }

   cout << "* Try push more than size = " << iStack.size() << " objects" << endl;
   try {
       // push/pop 4 objects
       for (int i = 0; i < iStack.size() - 1; i++) {
           iStack.push(i*2);
           cout << "Pushed object. " << " iStack current size = " << iStack.count() << " value = " << iStack.operator[](i) << endl;
       }
    } catch (MyStackIndexError &err) {
       cout << "\nCannot Push object #" << err.index << " because size is " << err.size << endl;
   }
   catch (out_of_range eRange) {
       cout << "\nPush object #" << last_index << " failed with message: " << eRange.what() << endl;
   }
   catch (...) {
       cout << "\nPush object #" << last_index << " failed!" << endl;
   }

    // MyString
    //С помощью шаблона MyStack создайте стек из 5 элементов int - iStack и
    //стек из 10 элементов MyString - strStack и поэкспериментируйте с функциями
    //push() и pop(), operator[]

   cout << "Create MyStack<string> with size 10" << endl;
   MyStack<string, 10> sStack;

   // push 5 elements
    try {
        sStack.push("one");
        sStack.push("two");
        sStack.push("three");
        sStack.push("four");
        sStack.push("five");
        cout << "sStack size = " << sStack.size() << "  sStack current size = " << sStack.count() << endl;

        // pop 2 elements
        string s1 = sStack.pop();
        string s2 = sStack.pop();
        cout << "s1 = " << s1 << " s2 = " << s2 << endl;
        cout << "sStack size = " << sStack.size() << "  sStack current size = " << sStack.count() << endl;

        // get element
    } catch (MyStackIndexError &err) {
    cout << "\nCannot Push object #" << err.index << " because size is " << err.size << endl;
    }
    catch (out_of_range eRange) {
        cout << "\nPush object #" << last_index << " failed with message: " << eRange.what() << endl;
    }
    catch (...) {
        cout << "Error" << endl;
    }
}

static void test_class_mystack2() {

    //Задание 2. Реализуйте шаблон стека - MyStack2 таким образом, чтобы
    //для хранения элементов использовался ОДНОСВЯЗНЫЙ список.
    //Реализуйте возможность распечатать элементы стека в том порядке, в котором их заносил (push())
    //пользователь
    cout << "* Test class MyStack2 template *" << endl;
    cout << "Create MyStack with size 5" << endl;
    MyStack2<int> iStack;
    cout <<"iStack current size = " << iStack.count() << endl;

    size_t last_index = 0;

    try {
        // push/pop 4 objects
        for (int i = 0; i < 10; i++) {
            iStack.push(1+i*2);
            cout << "Pushed object #" << i << " iStack current size = " << iStack.count() << " pushed value = " << iStack.operator[](0) << endl;
        }

        const int pop_value = iStack.pop();
        cout << "Pop last object." << " iStack current size = " << iStack.count() << endl;

        // get current index
        // get [] correct/incorrect objects
        last_index = iStack.count() - 1;
        cout << "Get object #" << last_index << ": { " << iStack.operator[](last_index) << " }" << endl;
        last_index++;
        cout << "Get object #" << last_index << ": { " << iStack.operator[](last_index) << " }" << endl;
    }
    catch (out_of_range eRange) {
        cout << "\nGet object #" << last_index << " failed with message: " << eRange.what() << endl;
    }
    catch (...) {
        cout << "\nGet object #" << last_index << " failed!" << endl;
    }

    cout << "Print MyStack2\n" << iStack << endl;
    cout << "Print revert MyStack2\n" << endl;
    iStack.revert_print(std::cout);
}

class Item
{
public:
	Item() { std::cout << "Item acquired\n"; }
	~Item() { std::cout << "Item destroyed\n"; }
};

void test_constructors_class_mystack() {

    cout << "* Test class MyStack constructors *" << endl;

    MyStack<string, 5> sStack1;
    sStack1.push("one");
    sStack1.push("two");
    sStack1.push("three");
    sStack1.push("four");

    cout << "Copy constructor " << endl;
    cout << "sStack1: \n" << sStack1 << endl;

    MyStack<string, 5> sStack2(sStack1);
    cout << "sStack2(copy of sStack1): \n" << sStack2 << endl;

    MyStack<string,5> sStack3, sStack4;
    sStack3.push("1");
    sStack3.push("2");
    sStack3.push("3");
    sStack3.push("4");
    sStack3.push("5");

    sStack4.push("zero");

    cout << "Copy operator = " << endl;
    cout << "sStack3: \n" << sStack3 << endl;
    cout << "sStack4: \n" << sStack4 << endl;
    sStack4 = sStack3;
    cout << "sStack4 = sStack3: \n" << sStack4 << endl;
    sStack3 = sStack1;
    cout << "sStack3 = sStack1: \n" << sStack3 << endl;

}

void test_constructors_class_mystack2() {

    cout << "* Test class MyStack2 constructors *" << endl;

    MyStack2<string> sStack1;
    sStack1.push("one");
    sStack1.push("two");
    sStack1.push("three");
    sStack1.push("four");

    cout << "Copy constructor " << endl;
    cout << "sStack1: \n" << sStack1 << endl;

    MyStack2<string> sStack2(sStack1);
    cout << "sStack2(copy of sStack1): \n" << sStack2 << endl;

    MyStack2<string> sStack3, sStack4;
    sStack3.push("1");
    sStack3.push("2");
    sStack3.push("3");
    sStack3.push("4");
    sStack3.push("5");

    sStack4.push("zero");

    cout << "Copy operator = " << endl;
    cout << "sStack3: \n" << sStack3 << endl;
    cout << "sStack4: \n" << sStack4 << endl;

    sStack4 = sStack3;
    cout << "sStack4 = sStack3: \n" << sStack4 << endl;

    sStack3 = sStack1;
    cout << "sStack3 = sStack1: \n" << sStack3 << endl;
}

int main() {

    test_function_template();
    test_class_mystack();
    test_class_mystack2();
    test_constructors_class_mystack();
    test_constructors_class_mystack2();
    return 0;
}
