#ifndef LAB2_SWAP_H
#define LAB2_SWAP_H

template<typename T> void Swap(T& value1, T& value2) {

    if (value1 == value2)
        return;

    T tmp(std::move(value1));
    value1 = std::move(value2);
    value2 = std::move(tmp);
}

#endif //LAB2_SWAP_H
