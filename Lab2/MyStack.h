#ifndef LAB2_MYSTACK_H
#define LAB2_MYSTACK_H

#include <cstddef>
#include <stdexcept>
#include <iostream>

struct MyStackIndexError {
    size_t index;
    size_t size;

    MyStackIndexError(size_t index, size_t size) {
        this->index = index;
        this->size = size;
    }
};

template<typename T = char, const size_t max_size = 16> class MyStack {
private:
    T array[max_size];
    size_t stack_size, stack_count;

public:
    MyStack() {
        stack_count = 0;
        stack_size = max_size;

        for (size_t i = 0; i < max_size; i++) {
            array[i] = T();
        }
    }
 
    size_t size() { return stack_size; }
    size_t count() { return stack_count; }

    void push(const T& value) {

        if (stack_count < max_size) {
            array[stack_count] = value;
            stack_count++;
        } else
            throw MyStackIndexError(stack_count, max_size);
    }

    const T& pop() {
        if (stack_count == 0)
            throw MyStackIndexError(stack_count, stack_count);

        stack_count--;
        return array[stack_count];
    }

    T& operator[](size_t index) {
        if (index >= stack_count)
            throw std::out_of_range("Empty object");

        return array[index];
    };

    const T& operator[](size_t index) const {
        if (index >= stack_count)
            throw std::out_of_range("Empty object");

        return array[index];
    };

    friend std::ostream& operator<<(std::ostream& os, const MyStack& my_stack) {
        for (size_t i = 0; i < my_stack.stack_count; i++) {
            os << "[" << i << "]: object { " << my_stack.array[i] << " }" << std::endl;
        }

        return os;
    }

};

#endif //LAB2_MYSTACK_H
