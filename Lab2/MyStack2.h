#ifndef LAB2_MYSTACK2_H
#define LAB2_MYSTACK2_H

#include <cstddef>
#include <stdexcept>
#include <iostream>
#include <utility>

template<typename T = char> class MyStack2 {

    class Node {
    private:
        T value;

    public:
        Node *pNext;

        Node() { pNext = nullptr; };

        Node(const T& value) : value { value } {
            pNext = nullptr;
        }

        Node(const Node& node) : value { node.value } {
            pNext = nullptr;
        }

        Node& operator=(const Node& node) {
            this->value = node.value;
            return *this;
        }

        virtual Node* clone() const {
            return new Node(*this);
        }

        virtual bool operator!=(const Node &node) const { return this->value != node.value; }
        virtual bool operator==(const Node &node) const { return this->value == node.value; }

        const T& get_obj() const { return value; }
    };

private:
    Node Head;
    size_t stack_count;

public:
    MyStack2() {
        stack_count = 0;
    }

    ~MyStack2() {
        Node *p_tmp = nullptr;
        for (Node *p_node = Head.pNext; p_node != nullptr;) {
            p_tmp = p_node;
            p_node = p_node->pNext;
            delete p_tmp;
            --stack_count;
        }
    }

    // copy constructor
    MyStack2(const MyStack2& stack) {
        this->stack_count = 0;
        Node *p_new = &(this->Head);

        for (Node *p_old = stack.Head.pNext; p_old != nullptr; p_old = p_old->pNext) {
            p_new->pNext = p_old->clone();
            p_new = p_new->pNext;
            this->stack_count++;
        }
    }

    // operator =
    MyStack2& operator=(const MyStack2& stack) {

        if (&stack == this)
            return *this;

        if (stack.stack_count < this->stack_count) {
            Node *p_node = nullptr;
            for (unsigned int i = this->stack_count; i > stack.stack_count; i--) {
                p_node = Head.pNext;

                if (p_node != nullptr) {
                    Head.pNext = p_node->pNext;
                    delete p_node;
                    this->stack_count--;
                }
            }
        } else if (stack.stack_count > this->stack_count) {
            Node *p_new = nullptr;

            for (unsigned int i = this->stack_count; i < stack.stack_count; i++) {
                p_new = new Node();
                p_new->pNext = this->Head.pNext;
                this->Head.pNext = p_new;
                this->stack_count++;
            }
        }

        for (Node *p_old = stack.Head.pNext, *p_new = this->Head.pNext;
             (p_old != nullptr) || (p_new != nullptr);
             p_old = p_old->pNext, p_new = p_new->pNext) {

            if (*p_new != *p_old)
                *p_new = *p_old;
        }

        return *this;
    }

    size_t count() { return stack_count; }

    void push(const T& value) {
        Node *p_push = nullptr;
        p_push = new Node(value);
        p_push->pNext = Head.pNext;
        Head.pNext = p_push;
        stack_count++;
    }

    T  pop() {

        if (stack_count == 0)
            throw std::out_of_range("No more objects");

        Node *p = Head.pNext;
        const T obj = p->get_obj();
        Head.pNext = p->pNext;
        delete p;
        stack_count--;
        return obj;
    }

    const T& operator[](size_t index) const {
        if (index >= stack_count)
            throw std::out_of_range("Empty object");

        size_t current_index = 0;

        for (Node *p = Head.pNext; p != nullptr; p = p->pNext) {
            if (current_index == index)
                return p->get_obj();

            current_index++;
        }

        throw std::out_of_range("Object not found");
    };

    friend std::ostream& operator<<(std::ostream& os, const MyStack2& my_stack) {
        size_t i = 0;

        for (Node *p = my_stack.Head.pNext; p != nullptr; p = p->pNext) {
            os << "[" << i++ << "]: object { " << p->get_obj() << " }" << std::endl;
        }

        return os;
    }

    void revert_print(std::ostream& os) {
        MyStack2<T> revert_stack;

        for (Node *p = Head.pNext; p != nullptr; p = p->pNext) {
            revert_stack.push(p->get_obj());
        }

        os << revert_stack;
    }

};

#endif //LAB2_MYSTACK_H
