//
// Created by User on 19.03.2021.
//

#ifndef CPP_LABS_210_PART10_H
#define CPP_LABS_210_PART10_H

#include <string>
#include <map>

template <typename T> std::map<std::string, T> EnumDict;

#endif //CPP_LABS_210_PART10_H
