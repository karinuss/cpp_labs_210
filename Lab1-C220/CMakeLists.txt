cmake_minimum_required(VERSION 3.16)
project(Lab1_C220)

set(CMAKE_CXX_STANDARD 17)

add_executable(Lab1_C220 lab1_c220.cpp part10.cpp part10.h)