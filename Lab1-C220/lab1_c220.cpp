//Практика 1:
// enum class
// initializer_list и универсальные списки инициализации
// auto
// decltype
// lambda
// template variable

#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <deque>
#include <map>
#include <algorithm>
#include <cmath>
#include <cstring>

using namespace std;

extern void part10();

template <typename T, typename C> std::ostream& operator<< (std::ostream& os, const pair<T, C> &p)
{
    os << p.first << " " << p.second << endl;
    return os;
}

template <typename T, typename C>  std::ostream& operator<< (std::ostream& os, set<T, C> s)
{
    for(auto iter:s)
        os << " " << iter;
    return os;
}

template <typename T, typename C, typename C0> std::ostream& operator<< (std::ostream& os, map<T, C, C0> m)
{
    for(auto iter:m)
        os << " " << iter;
    return os;
}

template <typename T>  std::ostream& operator<< (std::ostream& os, vector<T> v)
{
    for(auto iter:v)
        os << " " << iter;
    return os;
}

#if 0
// Задание 4
template <typename T> void PrintAnyCont(const T& t) {
    for(auto iter:t)
        cout << " " << iter;
    cout << endl;
}
#endif

// Задание 4

template <typename T> void PrintAnyCont(const T& t)
{
    copy(cbegin(t), cend(t), ostream_iterator<decltype(*begin(t))>(cout, " * "));
    cout << endl;
}


// Задание 5
// перегрузить унарный минус для стринг
template <typename T> void negate_all (T& a)
{  a = -a; }
template <> void negate_all (string& s)
{
    for_each(begin(s), end(s), [] (char &c)
    {
        if(!isalpha(c))
            return;
        c = (isupper(c) != 0) ? std::tolower(c) : std::toupper(c);
    });
}
// диапазонный for
template <typename T> void NegateAll(T& t) {
    for_each(begin(t), end(t), negate_all<decltype(*begin(t) + *begin(t))>);
}

#if 0
// Задание 6
template <typename T> struct comp {
    bool operator() (const T& a, const T& b) const
    {  return abs(a) < abs(b); }
};
template <typename T> void absSort(T& t)
{
   sort(begin(t), end(t), comp<decltype(*begin(t) + *begin(t))>());
}
#endif

// Задание 6
template <typename T> void absSort(T& t) {
    sort(begin(t), end(t), [] (auto &a, auto& b) -> bool const { return abs(a) < abs(b); });
}

// Задание 7
template <typename T1, typename T2> auto SumCont(const T1& t1, const T2& t2)
{
    size_t v_size = max(size(t1), size(t2));
    vector<decltype(*begin(t1) + *begin(t2))> v(v_size);
    copy(cbegin(t1), cend(t1), v.begin());
    transform(cbegin(t2), cend(t2), v.cbegin(), v.begin(), [] (const auto& a, const auto& b) { return a+b;});
    return v;
}
/*
template <typename T1, typename T2> auto SumCont(const T1& t1, const T2& t2)
{
    unsigned int v1_size = 0, v2_size = 0;

    // get size
    for(auto i:t1) v1_size++;
    for(auto i:t2) v2_size++;

    unsigned int v_size = (v1_size > v2_size) ? v1_size : v2_size;
    vector<decltype(*begin(t1) + *begin(t2))> v;
    v.reserve(v_size);

    auto plus_lambda = [] (const auto& a, const auto& b) { return a+b; };

    if (v1_size > v2_size)
    {
        transform(begin(t2), end(t2), begin(t1), back_insert_iterator<decltype(v)>(v), plus_lambda);
        auto bg = begin(t1);
        advance(bg, v2_size);
        copy(bg, end(t1), back_insert_iterator<decltype(v)>(v));
    }
    else
    {
        transform(begin(t1), end(t1), begin(t2), back_insert_iterator<decltype(v)>(v), plus_lambda);
        auto bg = begin(t2);
        advance(bg, v1_size);
        copy(bg, end(t2), back_insert_iterator<decltype(v)>(v));
    }

    return v;
}*/

// Задание 8
template <typename Tin, typename Tout1, typename Tout2, typename C>
void Separate(Tin& in1, Tout1& out1, Tout2& out2, C cond)
{
    copy_if(cbegin(in1), cend(in1), inserter(out1, out1.end()), cond);
    copy_if(cbegin(in1), cend(in1), inserter(out1, out1.end()), [&cond](auto& a) -> bool { return !cond(a); });
}

int main()
{
    //Задание 1.
    {
        cout << "\nЗадание 1\n";
        enum class months:unsigned char{ January, February, March/*...*/ };
        enum class weekDays:unsigned char { Monday, Tuesday /*...*/ };

        months m = months::January;
        //а) уберите проблемы (это значит, что КОМПИЛЯТОР не
        //должен позволять программисту осуществлять опасные
        //сравнения!)

        if (static_cast<weekDays>(m) == weekDays::Monday) {
        }

        //б) оптимизируйте использование памяти
        months year[] = { months::January, months::February, months::March };
        size_t n = sizeof(year); //???
        cout << "sizeof(year) = " << sizeof(year) << endl;
    }


/**********************************************************/
    //Задание 2. Создайте и с помощью списка инициализации
    //заполните значениями вектор с элементами - string
    //С помощью range-based for преобразуйте все буквы в
    //верхний регистр а остальные символы должны остаться
    //неизменными
    {
        cout << "\nЗадание 2\n";
        vector<string> vStr1{"rE", "S2fert", "!TWrty", "t+ery", "Y3gw="};

        cout << "vStr1 created: " << vStr1 << endl;
        for (auto &s:vStr1)
        {
           // for_each(s.begin(), s.end(), [](char& c) { if (isalpha(c) != 0) c = std::toupper(c); });
            for(auto &c:s)
            {
                c = std::toupper(c);
            }
        }

        cout << "vStr1: " << vStr1 << endl;
    }

/**********************************************************/
    ///Задание 3. Создайте и заполните значениями
    //map таким образом, чтобы он содержал в качестве ключа
    //букву, а в качестве значения отсортированную по
    //алфавиту совокупность слов (string), начинающихся с
    //этой буквы
    {
        cout << "\nЗадание 3\n";
        vector<string> vStr2 = {"ad", "qw", "Bs", "Ae4t", "Cets", "RT", "Jh", "yT", "WWW", "rOOt"};
        cout << "vStr2 created: " << vStr2 << endl;
        map<char, set<string>> map1;

        for (auto s:vStr2)
            map1[std::tolower(s[0])].insert(s);

        cout << "map1: \n" << map1 << endl;
    }
    //3а. Проинициализируйте map "вручную" посредством списка инициализации
    //С помощью range-based for и structured binding
    //распечатайте содержимое, например: A: any, apple, away
    {
        cout << "\nЗадание 3a\n";
        map<char,set<string>> map2 = { {'A', {"Apple", "Any", "away"}},
                                       {'F', {"float", "fuel", "fill"}},
                                       { 'B', {"back", "board"}},
                                       { 'C', {"coffee", "camera", "car", "const"}},
                                       { 'J', {"joke", "jack"}}
                                     };

      //  cout << "map2: \n" << map2 << endl;
      for (const auto& [letter, words]:map2)
          cout << letter << ": " << words << " # ";

    }

    //3б. Создайте пустой map и используйте заданный массив
    //для выполнения задания.
    //С помощью range-based for и structured binding
    //распечатайте содержимое, например: A: any, apple, away
    {
        //дано (например):
        cout << "\n\nЗадание 3б\n";
        const char* str[] = { "yong", "away", "bar", "any", "son", "apple" };
        map<char,set<string>> map3;

        for(auto s:str)
            map3[std::tolower(s[0])].insert(s);

        for (const auto& [letter, words]:map3)
            cout << letter << ": " << words << " # ";
    }


/*********************************************************/
    //Задание 4. создать функцию для вывода на печать
    //элементов последовательностей, заданных ниже:
    {
        cout << "\n\nЗадание 4\n";

        std::vector<double> vd = { 1.1,2.2,3.3 };
        PrintAnyCont(vd);

        std::string s("abc");
        PrintAnyCont(s);

        int ar[] = { 1, 2, 3 };
        PrintAnyCont(ar);

        std::initializer_list<int> il{ 3,4,5 };
        PrintAnyCont(il);

    }


/********************************************************/
    ///Задание 5.
    //Cоздать функцию для "отрицания" значений, например:
    //было: {1, -2, 5}, стало: {-1, 2, -5})
    //изменение объектов типа std::string может выглядеть "aBc1" -> "AbC1"
    //элементов последовательностей, заданных ниже:
    {
        cout << "\n\nЗадание 5\n";
        std::vector<double> vd{ 1.1, 2.2, 3.3, -6.2, -5.6 };
        cout << "vd: "; PrintAnyCont(vd);
        NegateAll(vd);
        cout << "vd negated: "; PrintAnyCont(vd);

        std::list<std::string> ls{ "aBc", "Qwerty", "n12" };
        cout << "ls: "; PrintAnyCont(ls);
        NegateAll(ls);
        cout << "ls negated: "; PrintAnyCont(ls);

        int ar[]{ 1, 2, 3, 0, -7, -8 };
        cout << "ar: "; PrintAnyCont(ar);
        NegateAll(ar);
        cout << "ar negated: "; PrintAnyCont(ar);

    }


/********************************************************/
    //Задание 6. Реализовать функцию сортировки по модулю
    //элементов последовательностей, заданных ниже
    //Собственно для сортировки можно использовать обобщенный
    //алгоритм sort(), а для задания условия - лямбда-функцию
    {
        cout << "\n\nЗадание 6\n";

        std::vector<double> vd = { -3.3,  2.2, -1.1 };
        cout << "vd: "; PrintAnyCont(vd);
        absSort(vd);
        cout << "vd sorted: "; PrintAnyCont(vd);

        int ar[] = { -3, 2, -1 };
        cout << "ar: "; PrintAnyCont(ar);
        absSort(ar);
        cout << "ar sorted: "; PrintAnyCont(ar);
    }


/********************************************************/
    //Задание 7.
    //Напишите функцию, которая будет формировать и
    //возвращать вектор, каждый элемент
    //которого является суммой элементов двух
    //последовательностей РАЗНОЙ длины
    //и с элементами РАЗНОГО типа.

    //Подсказка 1: так как последовательности могут быть
    //разной длины, логично сделать размер результирующего
    //вектора максимальным из двух

    //Подсказка 2: подумайте о возможности использования
    //алгоритмов copy() и transform(), в котором
    //трансформирующее действие удобно в свою очередь
    //задавать лямбда-функцией

    //например:
    {
        cout << "\n\nЗадание 7\n";

        std::vector<int> v{ 1,2,3,4 };
        std::list<double> l{ 1.1, 2.2, 3.3, 4.4, 5.5 };

        cout << "vector<int> v: "; PrintAnyCont(v);
        cout << "list<double> l: "; PrintAnyCont(l);

        auto v1 = SumCont(v, l);
        cout << "auto v1: "; PrintAnyCont(v1);
        cout << endl;

        std::list<int> ll{ 1, 2, 3, 4, 5, 6, 7, 8 };
        double ar[] = { 1.1, 2.2, 3.3, 4.4, 5.5 };

        cout << "list<int> ll: "; PrintAnyCont(ll);
        cout << "double ar[]: "; PrintAnyCont(ar) ;

        auto v2 = SumCont(ar, ll);
        cout << "auto v2: "; PrintAnyCont(v2);
        cout << endl;

        std::set<std::string> s{ "abc", "qwerty", "my"};
        std::deque<const char*> d{ "111", "22" };

        cout << "set<std::string> s: "; PrintAnyCont(s);
        cout << "deque<const char*> d: "; PrintAnyCont(d);

        auto v3 = SumCont(s, d);

        cout << "auto v3: "; PrintAnyCont(v3);
        cout << endl;
    }

/********************************************************/
    //Задание 8. Реализуйте функцию, которая принимает следующие параметры:
    //сформированную последовательность любого типа с элементами любого типа,
    //два (пустых) контейнера любого типа из vector, list, deque, set
    //с элементами того же типа, что и у сформированной последовательности

    //Функция должна "разложить" значения заданной последовательности в два пустых контейнера
    //согласно заданному условию. Условие задать лябда-функцией
    //Исходная последовательность при этом не меняется
    {
        cout << "\n\nЗадание 8\n";

        //Например:
        std::vector<int> v{ 1,2,3,4,5 };
        std::list<int> l; //сюда четные
        std::deque<int> d; //а сюда нечетные

        Separate(v, l, d, [] (const auto a) -> bool { if (a%2 != 0) return true;
                                                                    else return false; });

        cout << "v: "; PrintAnyCont(v); cout << endl;
        cout << "l: "; PrintAnyCont(l); cout << endl;
        cout << "d: "; PrintAnyCont(d); cout << endl;
    }


/********************************************************/
    //Задание 9. C помощью алгоритма for_each()!!!
    //(а не count_if()) посчитать сколько букв в верхнем
    //регистре.
    //  Использовать лямбда функцию
    {
        cout << "\n\nЗадание 9\n";
        char s[] = "Hello World!";
        //for_each
        unsigned int count = 0;
        for_each(begin(s), end(s), [&count](char c) { if (isupper(c) != 0) count++; });

        cout << "string = " << s << " capital letter count = " << count;
   }

/********************************************************/
    part10();

    return 0;
}
