//
// Created by User on 19.03.2021.
//
#include <stdexcept>
#include <algorithm>
#include "part10.h"

using namespace std;

enum class Colors : unsigned char {
    Blue, Red, White, Green, Yellow
};
enum class Days : int {
    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
};

template <> map<string, Colors> EnumDict<Colors> = {{ "blue", Colors::Blue},
                                                    { "red",  Colors::Red },
                                                    { "white", Colors::White },
                                                    { "green", Colors::Green },
                                                    { "yellow", Colors::Yellow }};

template <> map<string, Days> EnumDict<Days> = {{ "monday", Days::Monday},
                                                { "tuesday", Days::Tuesday},
                                                { "wednesday", Days::Wednesday},
                                                { "thursday", Days::Thursday},
                                                { "friday", Days::Friday},
                                                { "saturday", Days::Saturday},
                                                { "sunday", Days::Sunday}};

// Задание 10
template <typename T>const string& enumToString(T en)
{
    for(auto& pair:EnumDict<T>)
    {
        if (pair.second == en) {
            return pair.first;
        }
    }

    throw std::out_of_range("Invalid argument");
}

template <typename T> T stringToEnum(const string& s)
{
   return EnumDict<T>.at(s);
}

void part10()
{

//Задание 10. Реализовать конвертацию enum в строковое представление  - enumToString
// и наоборот - stringToEnum

//Подсказки:
//***********
//1. Соответствующие именованным константам строки все равно нужно где-то хранить =>
//1.1 Именованные константы в перечислении должны быть уникальными => соответствующие строки
//тоже должны быть уникальными, => уникальные строки могут быть использованы в качестве
//ключа в std::map
// ? для <COLORS> -> "blue" Ключ - "blue"

//1.2 а вот значения (соответствующие именованым константам)
//могут быть любыми и даже могут повторяться (упрощаем задачу, считая, что значения НЕ повторяются)
//=> подходит контейнер std::map<string,<значение> >
//1.3 Согласно стандарту С++11 переменные перечислимого типа могут быть разного размера и типа,
//а хотелось бы обеспечить универсальную конвертацию

//***********
//2. Так как типы перечислений разные, то enumToString и stringToEnum должны быть шаблонными
//2.1 Пользователь может ошибиться или злонамеренно передать в функции значения, которым
//   ничего не соответствует => защита!
//2.2 В функциях enumToString и stringToEnum (в зависимости от типа перечисления) нужно будет
//   анализировать соответствующий используемому типу перечисления контейнер

//***********
//3. Чтобы действия с map<string, <значение> > не зависили от типа перечисления, подумайте над использованием
//шаблонной переменной (в частности вспомните о возможности специялизации шаблонной переменной)


//***********

    {
//Например:

		Colors c1;
		try {
			c1 = stringToEnum<Colors>("blue");
            Colors c2 = stringToEnum<Colors>("f");
		}
		catch (...)
		{
		//...
		}


		auto Str = enumToString(c1);
		int e = 0;
    }
}