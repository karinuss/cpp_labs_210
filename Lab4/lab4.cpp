// Контейнеры STL:
//stack, queue, priority_queue
//set, multiset, map, multimap

#include <stack>
#include <queue>
#include <set>
#include <map>
#include <list>
#include <vector>
#include <string>
#include <clocale>

#include <iostream>
#include <cstring>

using namespace std;

class Point {
private:
    float x, y;

public:
    Point() { this->x = 0; this->y = 0; }
    Point(float x, float y) { this->x = x; this->y = y; }

    virtual bool operator<(const Point &point) const {  // for container 'set'
        float d_point = point.x * point.x + point.y * point.y;
        float d = this->x * this->x + this->y * this->y;
        return (d_point > d);
    }

    // for remove()
    //virtual bool operator==(const Point &point) const { return (this->x == point.x && this->y == point.y); }

    // predicate for remove_if
    //static bool below_zero(const Point &point) { return (point.x < 0 || point.y < 0); }

    friend std::ostream& operator<<(std::ostream& os, const Point& point) {
        os << "{ " << point.x << ", " << point.y << " }";
        return os;
    }
};


template <typename T> std::ostream& print_values(std::ostream& os, const T start, const T end)
{
    for(T iter = start; iter != end; ++iter)
        os << *iter << " ";

    return os;
}

template <typename T, typename C, typename C0> std::ostream& print_multimap_key(std::ostream& os, const multimap<T, C, C0> &m, T key)
{
    if (m.find(key) == m.end())
        return os;

    typename multimap<T, C, C0>::const_iterator start = m.lower_bound(key),
                                                end = m.upper_bound(key);
    os << start->first << ": ";

    for( ; start != end; ++start)
        os << start->second << " ";

    os << endl;
    return os;
}


template <typename T, typename C> std::ostream& operator<< (std::ostream& os, const pair<T, C> &p)
{
    os << p.first << " " << p.second << endl;
    return os;
}

template <typename T, typename C> std::ostream& operator<< (std::ostream& os, const multiset<T, C> &m)
{
    print_values(os, m.cbegin(), m.cend());
    return os;
}

template <typename T, typename C, typename C0> std::ostream& operator<< (std::ostream& os, map<T, C, C0> m)
{
    os << "\n ";
    print_values(os, m.cbegin(), m.cend());
    return os;
}

template <typename T, typename C, typename C0> std::ostream& operator<< (std::ostream& os, multimap<T, C, C0> m)
{
    os << "\n ";
    print_values(os, m.cbegin(), m.cend());
    return os;
}

template <typename T>  std::ostream& operator<< (std::ostream& os, vector<T> v)
{
    print_values(os, v.cbegin(), v.cend());
    return os;
}

template <typename T, typename C>  std::ostream& operator<< (std::ostream& os, set<T, C> v)
{
    print_values(os, v.cbegin(), v.cend());
    return os;
}

template <typename T, typename C> std::ostream& operator<< (std::ostream& os, stack<T, C> s)
{
    while (!s.empty()) {
        os << " " << s.top();
        s.pop();
    }

    return os;
}

template <typename T, typename C> std::ostream& operator<< (std::ostream& os, queue<T, C> q)
{
    while (!q.empty()) {
        os << " " << *(q.front());
        q.pop();
    }

    return os;
}

template <typename C> std::ostream& operator<< (std::ostream& os, priority_queue<const char*, vector<const char*>, C> q)
{

    while (!q.empty()) {
        os << " " << q.top();
        q.pop();
    }

    return os;
}

template <typename T>  std::ostream& operator<< (std::ostream& os, queue<Point*, T> q)
{
    while (!q.empty()) {
        os << " " << *(q.front());
        q.pop();
    }

    return os;
}

template <typename T> void print_info(const T &obj, const string name)
{
    cout << "\n" << name << ":" << " size " << obj.size();
    cout << "\n" << name << ": ";
    cout << obj << endl;
}

int main() {
    //Напишите шаблон функции для вывода значений stack, queue, priority_queue
    //обратите внимание на то, что контейнеры предоставляют РАЗНЫЕ методы для
    //получения значений

    ////////////////////////////////////////////////////////////////////////////////////
    //stack
    //Создайте стек таким образом, чтобы
    //а) элементы стека стали копиями элементов вектора
    //б) при выводе значений как вектора, так и стека порядок значений был одинаковым
    cout << "\n\n*** Stack ***\n";

    int iMas[] = {0, 1, 2, 3, 4, 5, 6, 7};
    vector<int> vInt1(iMas, iMas + sizeof(iMas) / sizeof(iMas[0]));
    print_info(vInt1, "vInt1");

    stack<int, vector<int>> svStack1(vector<int>(vInt1.rbegin(), vInt1.rend()));
    print_info(svStack1, "svStack1(vInt1)");

    ////////////////////////////////////////////////////////////////////////////////////
    //queue
    //Создайте очередь, которая содержит указатели на объекты типа Point,
    //при этом явно задайте базовый контейнер.
    cout << "\n\n*** Queue ***\n";
    queue<Point *, list<Point *>> pQueue1;
    pQueue1.push(new Point(19., 67.));
    pQueue1.push(new Point(5.5, 78.7));
    pQueue1.push(new Point(73.2, 0.9));
    pQueue1.push(new Point(18., 3.35));
    pQueue1.push(new Point(4.5, 8.7));
    pQueue1.push(new Point(3.2, 0.9));
    pQueue1.push(new Point(8.3, 30.0));
    pQueue1.push(new Point(8.0, 0.5));

    print_info(pQueue1, "pQueue1(Point *, list<Point*>)");

    //Измените значения первого и последнего элементов посредством front() и back()
    *(pQueue1.front()) = Point(10, 10);
    *(pQueue1.back()) = Point(11, 11);
    print_info(pQueue1, "pQueue1(use front(Point(10, 10)) and back(Point(11, 11)))");

    //Подумайте, что требуется сделать при уничтожении такой очереди?
    // delete Point* after use
    while (!pQueue1.empty()) {
       Point *tmp = pQueue1.front();
       pQueue1.pop();
       delete tmp;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //priority_queue
    //а) создайте очередь с приоритетами, которая будет хранить адреса строковых литералов - const char*
    //б) проинициализируйте очередь при создании с помощью вспомогательного массива с элементами const char*
    cout << "\n\n*** Priority queue ***\n";
    const char* cMas[] = {"b", "bB", "d", "coffee", "h", "a", "Hjkl", "f", "cofe", "g", "e"};
    cout << "cMas: ";
    print_values(std::cout, cMas, cMas+sizeof(cMas)/sizeof(cMas[0]));

    struct comp {
        bool operator() (const char *a, const char *b) const
        {
            return (strcmp(a,b) > 0) ? true : false;
        }
    };

    priority_queue<const char *, vector<const char *>, comp> cPrioQueue1(cMas, cMas + sizeof(cMas)/sizeof(cMas[0]));

    //в) проверьте "упорядоченность" значений (с помощью pop() ) - если они оказываются не упорядоченными, подумайте:
    //		что сравнивается при вставке?

    print_info(cPrioQueue1, "cPrioQueue1");
    cPrioQueue1.pop();
    print_info(cPrioQueue1, "cPrioQueue1");
    ////////////////////////////////////////////////////////////////////////////////////
    //set
    //a) создайте множество с элементами типа Point - подумайте, что необходимо определить
    //		в классе Point (и каким образом)
    // ответ: operator <
    cout << "\n\n*** Set ***\n";
    set<Point> pointSet;
    pointSet.insert(Point(10, 2.9));
    pointSet.insert(Point(1.0, 9));
    pointSet.insert(Point(6.7, 2.7));
    pointSet.insert(Point(7, 2));
    pointSet.insert(Point(-5.4, 2.6));
    pointSet.insert(Point(8, 3.));
    pointSet.insert(Point(-1.4, -12.5));

    //б) распечатайте значения элементов с помощью шаблона, реализованного в предыдущей лаб. работе
    print_info(pointSet, "pointSet(Point)");

    //в) попробуйте изменить любое значение...
    //
    set<Point>::iterator find_iter = pointSet.find(Point(8,3));
    find_iter = pointSet.erase(find_iter);
    pointSet.insert(find_iter, Point(555,555));
    print_info(pointSet, "pointSet(change Point(8,3) -> Point(555,555)");

    //г) Создайте два множества, которые будут содержать одинаковые значения
    //		типа int, но занесенные в разном порядке
    int iSetMas1[] = { 1, 7, 6, 2, 4, 8, 10, 15, 18 };
    int iSetMas2[] = { 4, 15, 8, 10, 7, 6, 18, 1, 2 };
    set<int> iSet1(iSetMas1, iSetMas1+sizeof(iSetMas1)/sizeof(iSetMas1[0]));
    set<int> iSet2(iSetMas2, iSetMas2+sizeof(iSetMas2)/sizeof(iSetMas2[0]));
    print_info(iSet1, "iSet1");
    print_info(iSet2, "iSet2");

    //д) Вставьте в любое множество диапазон элементов из любого другого
    //	контейнера, например, элементов массива	(что происходит, если в массиве имеются дубли?)
    int iSetMas11[] = { 1, 7, 6, 2, 4, 8, 10, 15, 18 };
    int iSetMas22[] = { 10, 1, 6, 99, 35 };
    set<int> iSet11(iSetMas11, iSetMas11+sizeof(iSetMas11)/sizeof(iSetMas11[0]));
    print_info(iSet11, "iSet11");
    iSet11.insert(iSetMas22, iSetMas22+sizeof(iSetMas22)/sizeof(iSetMas22[0]));
    cout << "iSetMas22: ";
    print_values(std::cout, iSetMas22, iSetMas22+sizeof(iSetMas22)/sizeof(iSetMas22[0]));
    print_info(iSet11, "iSet11 after insert iSetMas22");

    ////////////////////////////////////////////////////////////////////////////////////
    //map, multiset
    //а) создайте map, который хранит пары "фамилия, зарплата" - pair<const char*, int>,
    //	при этом строки задаются строковыми литералами
    //б) заполните контейнер значениями посредством operator[] и insert()
    //в) распечатайте содержимое

    map<const char*, int> famMap;
    famMap["Ivanov1"] = 5;
    famMap["Ivanova"] = 34;

    famMap.insert(pair<const char *, int>("Petrov", 30));
    famMap.insert(pair<const char *, int>("Petrov2", 60));
    famMap.insert(pair<const char *, int>("Petrov1", 50));

    print_info(famMap, "famMap");
    //г) замените один из КЛЮЧЕЙ на новый (была "Иванова", вышла замуж => стала "Петрова")

    map<const char*, int>::iterator it = famMap.find("Ivanova");

    if (it != famMap.end()) {
        pair<const char *, int> tmp = *it;
        tmp.first = "Sidorova";
        it = famMap.erase(it);
        famMap.insert(tmp);
    }

    print_info(famMap, "famMap(change Ivanova -> Sidorova)");

    //д) Сформируйте любым способом вектор с элементами типа string.
    //Создайте (и распечатайте для проверки) map<string, int>, который будет
    //содержать упорядоченные по алфавиту строки и
    //количество повторений каждой строки в векторе
    string sMas[] = {"ha","eb","yc", "yc", "ye", "ye", "td", "ye", "qf", "tg", "tg", "rh"};
    vector<string> vStr(sMas, sMas+8);
    print_info(vStr,"vStr created");

    map<string, int> mvStr;
    map<string, int>::iterator it_find;

    for(vector<string>::iterator it1 = vStr.begin(); it1 != vStr.end(); ++it1)
    {
       mvStr[*it1]++;
    }
    print_info(mvStr,"mvStr after add vector");

    //е)
    //задан массив:
    //const char* words[] = {"Abba", "Alfa", "Beta", "Beauty" ,...};
    //создайте map, в котором каждой букве будет соответствовать совокупность
    //лексиграфически упорядоченных слов, начинающихся с этой буквы.
    //Подсказка: не стоит хранить дубли одной и той же строки
    struct comp_asc_order:comp {
        bool operator()(const char *a, const char *b) const {
                return !(comp::operator()(a,b));
        }
    };

    const char* words[] = {"Abba", "Alfa", "Beta", "Beauty" , "Cafe", "Coffee"};
    //'A' -  "Abba" "Alfa"
    //'B' -  "Beauty" "Beta"  ...
    //...
    //map<char, set<const char *, comp_asc_order>> mvStr2;
    map<char, set<const char *, comp_asc_order>> mvStr2;
    print_info(mvStr2,"mvStr2 created");
    for(unsigned i = 0; i < sizeof(words)/sizeof(words[0]); i++)
    {
        mvStr2[words[i][0]].insert(words[i]);
    }

    print_info(mvStr2,"mvStr2 after add words");

    //ж)
    //создайте структуру данных, которая будет хранить информацию о студенческих группах.
    //Для каждой группы должны хранится фамилии студентов (по алфавиту). При этом
    //фамилии могут дублироваться
    //Сами группы тоже должны быть упорядочены по номеру
    //
    //номера
    const char* students[] = { "Ivanov", "Petrov", "Sidorov", "Lavrov", "Korablev", "Losev", "Yakunin"};
    map<unsigned int, multiset<const char *, comp_asc_order>, std::greater<unsigned int>> mStudGroups;

    mStudGroups[33].insert(students+1, students+3);
    mStudGroups[10].insert(students+2, students+4);
    mStudGroups[11].insert(students, students+5);
    mStudGroups[22].insert(students+5, students+6);
    mStudGroups[33].insert(students, students+3);
    mStudGroups[22].insert(students+4, students+6);

    print_info(mStudGroups, "mStudGroups");

    ////////////////////////////////////////////////////////////////////////////////////
    //multimap
    //а) создайте "англо-русский" словарь, где одному и тому же ключу будут соответствовать
    //		несколько русских значений - pair<string,string>, например: strange: чужой, странный...
    //б) Заполните словарь парами с помощью метода insert или проинициализируйте с помощью
    //		вспомогательного массива пара (пары можно конструировать или создавать с помощью шаблона make_pair)
    //в) Выведите все содержимое словаря на экран
    //г) Выведите на экран только варианты "переводов" для заданного ключа. Подсказка: для нахождения диапазона
    //		итераторов можно использовать методы lower_bound() и upper_bound()

    multimap<string, string, std::less<string>> en_ru_dict {
        {"strange", "чужой"}, {"strange", "странный"}};

    en_ru_dict.insert({"big", "большой"});
    en_ru_dict.insert({"big", "крупный"});
    en_ru_dict.insert({"big", "важный"});
    pair<string, string> en_ru_word[] = {{"red", "красный"}, {"red", "рыжий"}, { "red", "алый"}};

    en_ru_dict.insert(en_ru_word, en_ru_word+sizeof(en_ru_word)/sizeof(en_ru_word[0]));

    print_info(en_ru_dict, "en_ru_dict");

    print_multimap_key(std::cout, en_ru_dict, string("big"));
    print_multimap_key(std::cout, en_ru_dict, string("red"));
    print_multimap_key(std::cout, en_ru_dict, string("strange"));
    print_multimap_key(std::cout, en_ru_dict, string("invalid value"));
    return 0;
}


