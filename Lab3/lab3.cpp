//Стандартная библиотека шаблонов - STL 
//Контейнеры стандартной библиотеки - vector
//Итераторы

#include <iostream>
#include <vector>
#include <list>
#include <deque>

using namespace std;

class Point {
private:
    float x, y;

public:
    Point() { this->x = 0; this->y = 0; }
    Point(float x, float y) { this->x = x; this->y = y; }

    virtual bool operator<(const Point &point) const {  // for sort()
        float d_point = point.x * point.x + point.y * point.y;
        float d = this->x * this->x + this->y * this->y;
        return (d_point > d);
    }

    // for remove()
    virtual bool operator==(const Point &point) const { return (this->x == point.x && this->y == point.y); }

    // predicate for remove_if
    static bool below_zero(const Point &point) { return (point.x < 0 || point.y < 0); }

    friend std::ostream& operator<<(std::ostream& os, const Point& point) {
        os << "{ " << point.x << ", " << point.y << " }";
        return os;
    }
};

template <typename T> const void print_values(const T start, const T end, const string name)
{
    cout << "\n" << name << ": ";
    for(T iter = start; iter != end; ++iter)
        cout << *iter << " ";
    cout << "\n";
}

template <> const void print_values(vector<Point*>::iterator start, vector<Point*>::iterator end, const string name)
{
    cout << "\n" << name << ": ";
    for(vector<Point*>::iterator iter = start; iter != end; ++iter) {
        cout << **iter << " ";
    }
    cout << "\n";
}

template <typename T> const void print_info(const T &v, const string name)
{
    cout << "\n" << name << ":" << " size " << v.size() << " max_size " << v.max_size();
    print_values(v.cbegin(), v.cend(), name);
}

template <typename T> const void print_info(const vector<T> &v, const string name)
{
    cout << "\n" << name << ":" << " size " << v.size() << " capacity " << v.capacity() << " max_size " << v.max_size();
    print_values(v.cbegin(), v.cend(), name);
}

template <typename T> const void print_info(const vector<vector<T>> &v, const string name)
{
    cout << "\n" << name << ":" << " size " << v.size() << " capacity " << v.capacity() << " max_size " << v.max_size();

    for(unsigned int i = 0; i < v.size(); ++i) {
          cout << "\n[" << i << "]: ";
          print_info(v.operator[](i), name);
    }
}

template <typename T> std::ostream& operator<< (std::ostream& os, const vector<T> &v)
{
    os << "\n" << "v" << ":" << " size " << v.size() << " capacity " << v.capacity() << " max_size " << v.max_size();

    for(unsigned int i = 0; i < v.size(); ++i) {
        os << v.operator[](i);
    }

    return os;
}

template <typename T> void print_info(const deque<T> &deq, const string name)
{
      cout << "\n" << name << ":" << " size " << deq.size()  << " max_size " << deq.max_size();
      cout << "\n";
      print_values(deq.cbegin(), deq.cend(), name);
}

  static void test_vector1()
  {
      cout << "\n*** Vector(first part) ***\n";
      // Контейнер стандартной библиотеки vector
      //Создание векторов.
      //1. Создайте следующие вектора:
      //---------------------------
      //пустой вектор целых чисел - vInt и проверьте с помощью функции size(),
      //что его размер нулевой.
      vector<int> vInt;
      print_info(vInt, "Created vector vInt");

      //2. Раскомментируйте следующий фрагмент, подумайте - все ли корректно
      //Если есть некорректности, - исправьте
      // Ответ:  контейнер пустой, нужно увеличить размер
      vInt.resize(1);
      print_info(vInt, "vInt resize to 1");
      vInt.front() = 1;
      print_info(vInt, "vInt");

      //3. С помощью функции push_back() в цикле заполните вектор какими-либо значениями.
      //На каждой итерации цикла следите за размером вектора с помощью методов
      //size(), capacity(), max_size() и выводите значения элементов.
      //
      //Подсказка: так как такая проверка понадобится Вам и в следующих заданиях,
      //напишите шаблон функции, которая для вектора, содержащего элементы любого типа
      //выводит его "реквизиты" и значения элементов на консоль.

      for (int i = 0; i < 24; ++i) {
          vInt.push_back(i);
          cout << "\nvInt.push_back() value " << i << " ";
          print_info(vInt, "vInt");
      }

      print_info(vInt, "vInt");

      //4. вектор вещественных - vDouble1 с заданным Вами начальным размером  и
      //проверьте результат с помощью созданного Вами шаблона. Как будут
      //проинициализированы элементы вектора?

      vector<double> vDouble1(10);
      print_info(vDouble1, "Created vector vDouble1");

      //5. вектор объектов типа MyString с начальным размером - 5 элементов
      //и инициализацией каждого элемента строкой "A"
      //C помощью функции at() а также с помощью оператора
      //индексирования []  измените значения каких-либо элементов.
      //Попробуйте "выйти" за границы вектора с помощью at() и
      //с помощью [].

      vector<string> vString(5, "A");
      print_info(vString, "Created vector vString");

      try {
          std::size_t size = vString.size();
          vString.operator[](size/2) = "K";
          print_info(vString, "vString[size/2] = 'K'");
          vString.at(1) = "O";
          print_info(vString, "vString.at(1) = 'O'");
          //cout << "try to set vString[size]" << endl; // генерирует ошибку, но не exception
          //vString.operator[](size) = "D";
          cout << "try to set vString.at(size)" << endl;
          vString.at(size) = "D"; // генерирует exception
      } catch (std::out_of_range e) {
          cout << "vString out of bounds: " << e.what();
      } catch (...) {
          cout << "vString exception";
      }

      //6. вектор вещественных - vDouble3, который является копией элементов
      // [0,5) массива вещественных чисел dMas. Предворительно массив dMas
      //нужно создать и проинициализировать!
      double dMas[] = { 0., 1., 2., 3., 4., 5. };
      vector<double> vDouble3(dMas, dMas+sizeof(dMas)/sizeof(dMas[0])-1);
      print_info(vDouble3, "Created vector vDouble3");

      //7. вектор вещественных - vDouble4, который является копией элементов
      // [2,5) вектора vDouble3.

      std::vector<double>::iterator it_start;
      std::vector<double>::iterator it_end;

      for(vector<double>::iterator iter = vDouble3.begin(); iter != vDouble3.end(); ++iter)
      {
          if (*iter == 2)
              it_start = iter;

          if (*iter == 4)
          {
              it_end = iter;
              break;
          }
      }

      vector<double> vDouble4(it_start, it_end);
      print_info(vDouble4, "Created vector vDouble4");

      //8. вектор элементов типа Point - vPoint1 а) с начальным размером 3. Какой конструктор
      //будет вызван для каждого элемента?
      //b) vPoint2 с начальным размером 5 и проинициализируйте каждый элемент координатами (1,1).
      vector<Point> vPoint1(3);
      print_info(vPoint1, "Created vector vPoint1");

      vector<Point> vPoint2(5, Point(1.,1.));
      print_info(vPoint2, "Created vector vPoint2");

      //9. вектор указателей на Point - vpPoint с начальным размером 5
      //Подумайте: как корректно заставить эти указатели "указывать" на объекты Point
      //Подсказка: для вывода на печать значений скорее всего Вам понадобится
      //а) специализация Вашей шаблонной функции
      //б) или перегрузка operator<< для Point*
      //Какие дополнительные действия нужно предпринять для такого вектора?
      // ответ: удалить Point* после использования
      vector<Point*> vpPoint(5);
      print_info(vpPoint, "Created vector vpPoint");
      Point *new_point1 = new Point(1,4);
      Point *new_point2 = new Point(6,89);
      vpPoint.push_back(new_point1);
      print_info(vpPoint, "vpPoint.push_back(new Point(1,4))");
      vpPoint.push_back(new_point2);
      print_info(vpPoint, "vpPoint.push_back(new Point(6,89))");

      delete new_point1;
      delete new_point2;
  }

  static void test_memory()
  {
      cout << "\n*** Memory allocation ***\n";
      //Резервирование памяти.
      //Подумайте, всегда ли верны приведенные ниже проверки?
      {
      size_t n = 110;
      vector<int> v(n);
      print_info(v, "Created vector v(n = 11)");
      v.resize(n/2);
      print_info(v, "Vector v.resize(n/2)");

          if(v.capacity() == n) //true?
              cout << "v.capacity() == n" << endl;
          else
              cout << "v.capacity() != n" << endl;

      v.resize(n*2);
      print_info(v, "Vector v.resize(n*2)");

          if(v.capacity() == n) //true?
              cout << "v.capacity() == n" << endl;
          else
              cout << "v.capacity() != n" << endl;

      // ответ: не всегда. resize изменяет размер vector, capacity всегда >= size
      // в случае уменьшения количества элементов capacity не уменьшается
      }

      {
      int n = 10;
      size_t m = 5;
      vector<int> v2(n);
      print_info(v2, "Created vector v2(n = 10)");
      v2.reserve(m);
      print_info(v2, "v2.reserve(m = 5)");

      if(v2.capacity() == m) //true?
          cout << "v2.capacity() == m(" << m << ")" << endl;
      else
          cout << "v2.capacity() != m(" << m << ")" << endl;

      m = 50;
      v2.reserve(m);
      print_info(v2, "v2.reserve(m = 50)");

      if(v2.capacity() == m) //true?
          cout << "v2.capacity() == m(" << m << ")" << endl;
      else
          cout << "v2.capacity() != m(" << m << ")" << endl;

      // ответ: не всегда. reserve увеличивает capacity vector, если она меньше или не изменяет ее в иных случаях
      }

      {
      vector<int> v3(3,5);
      print_info(v3, "Created vector v3(3, 5)");
      v3.resize(4,10); //значения? ответ: добавляет 4тый элемент со значением 10
      print_info(v3, "v3.resize(4,10)");
      v3.resize(5); //значения? ответ: расширяет вектор до 5 элеметнов, дефолтное значение 5го элемнта равно 0
      print_info(v3, "v3.resize(5)");
      }

      //Создайте два "пустых" вектора с элементами
      //любого (но одного и того же типа) типа.
      //В первом векторе зарезервируйте память под 5 элементов, а потом заполните
      //его значениями с помощью push_back.
      //Второй вектор просто заполните значениями посредством push_back.
      //
      //Сравните размер, емкость векторов и значения элементов
      vector<float> vf1;
      print_info(vf1, "Created vector vf1");
      vf1.reserve(5);
      print_info(vf1, "vf1.reserve(5)");
      for (int i = 0; i < vf1.capacity(); i++)
          vf1.push_back(i);

      print_info(vf1, "vf1.push_back");

      vector<string> vs1;
      print_info(vs1, "Created vector vs1");
      vs1.push_back("t1");
      vs1.push_back("r3");
      vs1.push_back("v5");
      vs1.push_back("m8");
      vs1.push_back("c7");

      print_info(vs1, "vs1.push_back");
  }

  template <typename T> void remove_equal_chars(vector<T> &v)
  {
      typename vector<T>::iterator iter = v.begin();
      bool is_found = false;
      typename vector<T>::iterator iter_start;
      typename vector<T>::iterator iter_end;
      for (typename vector<T>::iterator iter_next = v.begin(); iter_next != v.end(); ) {
          ++iter_next;

          if(!is_found)
          {
              if (*iter == *iter_next)
              {
                  iter_start = iter;
                  is_found = true;
              }
          } else
          {
              if (*iter != *iter_next)
              {
                  iter_end = iter_next;
                  iter_next = v.erase(iter_start, iter_end);
                  is_found = false;
                //  print_values(v.begin(), v.end(), "erase");
              }
          }

          iter = iter_next;
      }
  }

  template <typename T> void unique_chars(vector<T> &v)
  {
      bool is_found = false;

      for (typename vector<T>::iterator iter1 = v.begin(); iter1 != v.end();) {
          is_found = false;
          typename vector<T>::iterator tmp = iter1;
          for (typename vector<T>::iterator iter2 = ++tmp; iter2 != v.end(); ) {

              if (*iter1 == *iter2) {
                  iter2 = v.erase(iter2);
                  is_found = true;
              } else {
                  ++iter2;
              }

              //print_values(v.begin(), v.end(), "erase");
          }

          if (is_found)
              iter1 = v.begin();
          else
              ++iter1;
      }
  }

  static void test_vector2()
  {

      cout << "\n*** Vector(second part) ***\n";
      //1. shrink_to_fit - Уменьшение емкости вектора.
      cout << "* shrink_to_fit - Уменьшение емкости вектора" << endl;
      //Для любого вектора из предыдущего задания требуется уменьшить емкость
      //до size.
      vector<string> vString(10, "K");
      vString.push_back("h");
      print_info(vString, "Created vector vString");

      vString.shrink_to_fit();
      print_info(vString, "vString.shrink_to_fit()");

      //2. Создание "двухмерного вектора" - вектора векторов
      cout << "* Создание \"двухмерного вектора\" - вектора векторов" << endl;

      //Задан одномерный массив int ar[] = {11,2,4,3,5};
      //Создайте вектор векторов следующим образом:
      //вектор vv[0] - содержит 11 элементов со значением 11
      //vv[1] - содержит 2,2
      //vv[2] - содержит 4,4,4,4
      //...
      //Распечатайте содержимое такого двухмерного вектора по строкам
      int ar[] = { 11, 2, 4, 3, 5 };
      vector<vector<int>> vvInt;
      print_info(vvInt, "Created vector vvInt");
      for (int i = 0; i < sizeof(ar)/sizeof(ar[0]); ++i)
      {
          vvInt.push_back(vector<int>(ar[i], ar[i]));
      }
      print_info(vvInt, "vvInt");

      //3. Вставка элемента последовательности insert().
      cout << "\n* Вставка элемента последовательности insert()." << endl;
      //В вектор vChar2 вставьте в начало вектора символ только при
      //условии, что в векторе такого еще нет.
      vector<char> vChar2;
      vChar2.push_back('c');
      vChar2.push_back('g');
      vChar2.push_back('b');
      vChar2.push_back('d');
      vChar2.push_back('l');
      vChar2.push_back('c');

      print_info(vChar2, "Created vector vChar2");

      char sign_for_search = 'f';
      bool is_finded = false;
      for (vector<char>::iterator iter = vChar2.begin(); iter != vChar2.end() ; ++iter) {
          if (*iter == sign_for_search)
          {
              is_finded = true;
              break;
          }
      }

      if (!is_finded)
          vChar2.insert(vChar2.begin(), sign_for_search);

      print_info(vChar2, "vector vChar2 after add 'f'");

      //4. Вставьте перед каждым элементом вектора vChar2 букву 'W'
      for (vector<char>::iterator iter = vChar2.begin(); iter != vChar2.end(); )
      {
          iter = vChar2.insert(iter, 'W');
          ++iter;
          ++iter;
      }

      print_info(vChar2, "vector vChar2 after add 'W'");

      //5. Напишите функцию, которая должна удалять только повторяющиеся последовательности.
      //Например: было - "qwerrrrty12222r3", стало - "qwety1r3"
      char char3_mas[] = {'q','q', 'w', 'e', 'r', 'r', 'r', 'r', 't', 'y', '1', '2', '2', '2', '2', 'r', '3','3' };
      vector<char> vChar3(char3_mas, char3_mas + sizeof(char3_mas));
      print_info(vChar3, "Create vector vChar3");
      remove_equal_chars(vChar3);
      print_info(vChar3, "remove_equal_chars(vChar3)");

      //6. Удаление элемента последовательности erase()
      //Напишите функцию удаления из любого вектора всех дублей
      //Например: было - "qwerrrrty12222r3", стало - "qwerty123"
      char char4_mas[] = {'q', 'q', 'w', 'e', 'r', 'r', 'r', 'r', 't', 'y', '1', '2', '2', '2', '2', 'r', '3' };
      vector<char> vChar4(char4_mas, char4_mas + sizeof(char4_mas));
      print_info(vChar4, "Create vector vChar4");
      unique_chars(vChar4);
      print_info(vChar4, "unique_chars(vChar4)");

      //7. Создайте новый вектор таким образом, чтобы его элементы стали
      //копиями элементов любого из созданных ранее векторов, но расположены
      //были бы в обратном порядке
      int iMas[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
      vector<int> vInt3(iMas, iMas+sizeof(iMas)/sizeof(iMas[0]));
      print_info(vInt3, "Created vector vInt3");

      vector<int> vInt3_reverse(vInt3.rbegin(), vInt3.rend());
      print_info(vInt3_reverse, "Created vector vInt3_reverse");
  }

  static void test_list()
  {
      cout << "\n*** List ***\n";

      //Задание 1. Списки. Операции, характерные для списков.
      cout << "* Задание 1. Списки. Операции, характерные для списков" << endl;
      //Создайте пустой список из элементов Point - ptList1 и наполните
      //его значениями с помощью методов push_back(),
      //push_front, insert()

      list<Point> ptList1;
      ptList1.push_back(Point(10, 2.9));
      ptList1.push_back(Point(1.0, 9));
      ptList1.push_front(Point(6.7, 2.7));
      ptList1.push_front(Point(7, 2));
      ptList1.push_front(Point(-5.4, 2.6));

      list<Point>::iterator insert_iter = ptList1.begin();
      ++insert_iter;
      ptList1.insert(insert_iter, Point(55, 55));

      print_info(ptList1, "ptList1");

      //1. Напишите шаблон функции, которая будет выводить элементы
      //ЛЮБОГО КОНТЕЙНЕРА на печать. Проверьте работу шаблона на контейнерах
      //vector и list. Подсказка - хотелось бы увидеть тип контейнера.

      print_info(ptList1, "ptList1");

      //2. Сделайте любой из списков "реверсивным" - reverse()
      list<Point> ptList1_reverse = ptList1;
      ptList1_reverse.reverse();

      print_info(ptList1_reverse, "ptList1_reverse (copy ptList1 and reverse)");

      //3. Создайте список ptList2 из элементов Point таким образом, чтобы он стал
      //копией вектора элементов типа Point, но значения элементов списка располагались
      //бы в обратном порядке
      list<Point> ptList2;

      for(list<Point>::reverse_iterator iter = ptList1.rbegin(); iter != ptList1.rend(); ++iter)
          ptList2.push_back(*iter);

      print_info(ptList2,"ptList2 (reverse ptList1)");

      //4. Отсортируйте списки  ptList1 и ptList2 - методом класса list - sort()
      //по возрастанию.
      //Подумайте: что должно быть перегружено в классе Point для того, чтобы
      //работала сортировка

      ptList1.sort();
      print_info(ptList1,"ptList1 (sort)");

      cout << "\nptList2 (sort)\n";
      ptList2.sort();
      print_info(ptList2, "ptList2 (sort)");

      //5. Объедините отсортированные списки - merge(). Посмотрите: что
      //при этом происходит с каждым списком.

      list<Point> ptList1_merge = ptList1;
      list<Point> ptList2_merge = ptList2;
      ptList1_merge.sort();
      ptList2_merge.sort();

      ptList1_merge.merge(ptList2_merge);
      print_info(ptList1_merge, "ptList1_merge (merge ptList1 with ptList2)");

      cout << "\nptList2_merge (merge ptList1 with ptList2)\n";
      print_info(ptList2_merge, "ptList2_merge (merge ptList1 with ptList2)");

      //6. Исключение элемента из списка - remove()
      //Исключите из списка элемент с определенным значением.
      //Подумайте: что должно быть перегружено в классе Point?

      ptList1_merge.remove(Point(1,9));
      print_info(ptList1_merge, "ptList1_merge (remove Point(1,9))");

      //7. Исключение элемента из списка, удовлетворяющего заданному условию:
      //любая из координат отрицательна - remove_if().
      ptList1_merge.remove_if(Point::below_zero);
      print_info(ptList1_merge, "ptList1_merge (remove below zero)");

      //8. Исключение из списка подряд расположенных дублей - unique().
      ptList1_merge.unique();
      print_info(ptList1_merge, "ptList1_merge (unique)");
  }

  static void test_deque()
  {
      cout << "\n*** Deque ***\n";

      //Задание 2.Очередь с двумя концами - контейнер deque
      cout << "* Задание 2.Очередь с двумя концами - контейнер deque" << endl;
      //Создайте пустой deque с элементами типа Point. С помощью
      //assign заполните deque копиями элементов вектора. С помощью
      //разработанного Вами в предыдущем задании универсального шаблона
      //выведите значения элементов на печать
      deque<Point> decPoint;
      vector<Point> vdecPoint(10);

      float x = 0., y = 0.;
      for (vector<Point>::iterator iter = vdecPoint.begin(); iter != vdecPoint.end(); ++iter)
      {
          *iter = Point(x,y);
          x += 2.;
          y += 3.;
      }

      print_info(vdecPoint, "Created vector vdecPoint");

      decPoint.assign(vdecPoint.begin(), vdecPoint.end());
      print_info(decPoint, "decPoint");

      //Создайте deque с элементами типа MyString. Заполните его значениями
      //с помощью push_back(), push_front(), insert()
      deque<string> deqString;
      print_info(deqString, "Create deqString");

      deqString.push_back("Art");
      deqString.push_back("Bde");
      deqString.push_back("Cer");
      deqString.push_front("agt");
      deqString.push_front("byh");
      deqString.push_front("cju");
      deqString.insert(deqString.cbegin(), 3, "LaK");
      deqString.insert(deqString.cend(), 3, "gAK");

      print_info(deqString, "deqString");

      //С помощью erase удалите из deque все элементы, в которых строчки
      //начинаются с 'A' или 'a'
      for (deque<string>::iterator iter = deqString.begin(); iter != deqString.end(); )
      {
          string s = *iter;

          if (s.rfind("a", 0) != -1 || s.rfind("A", 0) != -1)
          {
              cout << "'" << s << "' will be erased" << endl;
              iter = deqString.erase(iter);
          }
          else
              ++iter;
      }

      print_info(deqString, "deqString after erased 'A' and 'a'");
  }

  int main()
  {
    //  test_vector1();
      test_vector2();
     // test_list();
      //test_deque();
      return 0;
  }