#ifndef LAB1_SHAPE_H
#define LAB1_SHAPE_H

#include <math.h>
#include <iostream>

using namespace std;

enum Colors {
    COLOR_NONE, COLOR_RED, COLOR_BLUE, COLOR_GREEN
};

class Shape {
protected:
    Colors color;

public:
    Shape() { color = COLOR_NONE; }
    Shape(const Shape &shape) { color = shape.color; }

    virtual Shape* clone() { return new Shape(*this); }

    virtual double get_area() const { return 0; };
    void set_color(Colors color) { this->color = color; }

    Colors get_color() { return this->color; }

    virtual bool operator==(const Shape &sh) const {
        return (color == sh.color);
    }

    virtual bool operator!=(const Shape &sh) const {
        return (color != sh.color);
    }

    friend ostream &operator<<(ostream &os, const Shape &sh) {
        return os << "{ color = " << sh.color << " }";
    }

    friend istream &operator>>(istream &is, Shape &shape) {
        string s;
        getline(is, s, '{');
        getline(is, s, '}');
        double area = 0;
        sscanf(s.c_str(), " color = %d ", &shape.color);
        return is;
    }
};

class Rect : public Shape {
private:
    double lx, ly, rx, ry;

public:
    Rect() {
        lx = ly = rx = ry = 0.0;
    };

    Rect(const Rect &rect) {
        this->lx = rect.lx;
        this->rx = rect.rx;
        this->ly = rect.ly;
        this->ry = rect.ry;
        this->color = rect.color;
    }

    Rect(double lx, double ly, double rx, double ry) {
        this->lx = lx;
        this->ly = ly;
        this->rx = rx;
        this->ry = ry;
    }

    virtual Rect* clone() { return new Rect(*this); }

    virtual double get_area() const;
    virtual bool operator==(const Shape &sh) const;
    virtual bool operator!=(const Shape &sh) const;

    friend ostream &operator<<(ostream &os, const Rect &rect) {
        return os << "{ color = " << rect.color << " (lx, ly, rx, ry) = (" << rect.lx << ", " << rect.ly \
 << ", " << rect.rx << ", " << rect.ry << ") area " << rect.get_area() << " }";
    }

    friend istream &operator>>(istream &is, Rect &rect) {
        string s;
        getline(is, s, '{');
        getline(is, s, '}');
        double area = 0;
        sscanf(s.c_str(), " color = %d (lx, ly, rx, ry) = (%lf, %lf, %lf, %lf) area %lf ",
               &rect.color, &rect.lx, &rect.ly, &rect.rx, &rect.ry, &area);
        return is;
    }

};

class Circle : public Shape {
private:
    double cx, cy;
    double radius;

public:
    Circle() {
        cx = cy = radius = 0.0;
    };

    Circle(double radius, double cx, double cy) {
        if (radius < 0)
            return;

        this->cx = cx;
        this->cy = cy;
        this->radius = radius;
    }

    Circle(const Circle &circle) {
        this->cx = circle.cx;
        this->cy = circle.cy;
        this->radius = circle.radius;
        this->color = circle.color;
    }

    virtual Circle* clone() { return new Circle(*this); }

    virtual double get_area() const;
    virtual bool operator==(const Shape &sh) const;
    virtual bool operator!=(const Shape &sh) const;

    friend ostream &operator<<(ostream &os, const Circle &circle) {
        return os << "{ color = " << circle.color << " (cx, cy, radius) = (" << circle.cx << ", " \
 << circle.cy << ", " << circle.radius << ") area " << circle.get_area() << " }";
    }

    friend istream &operator>>(istream &is, Circle &circle) {
        string s;
        getline(is, s, '{');
        getline(is, s, '}');
        double area = 0;
        sscanf(s.c_str(), " color = %d (cx, cy, radius) = (%lf, %lf, %lf) area %lf ",
               &circle.color, &circle.cx, &circle.cy, &circle.radius, &area);
        return is;
    }
};

#endif //LAB1_SHAPE_H
