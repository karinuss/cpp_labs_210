#ifndef LAB1_LIST_H
#define LAB1_LIST_H

#include <iostream>

using namespace std;

#include "shape.h"

class List {
    class Node {

    private:
        Shape *pShape;
    public:
        Node *pPrev;
        Node *pNext;
        Node();
        Node(const Node& node);
        Node(Node *pPrev, Shape *pShape);
        ~Node();
        Shape *get_shape() { return pShape; }
        void set_shape(Shape *shape) { this->pShape = shape; }

        Node* clone() const { return new Node(*this); }
        Node& operator=(const Node& node);
    };

private:
    Node Tail;
    Node Head;
    size_t m_size;

    void add(Shape *shape, Node *node);
    void swap_nodes(Node *node1, Node *node2);

public:
    List();
    ~List();

    void add_tail(Shape *shape);
    void add_head(Shape *shape);
    void remove(Shape *shape);
    void sort_by_area();

    size_t get_size() { return m_size; }

    friend ostream &operator<<(ostream &os, const List &list);
    friend istream &operator>>(istream &is, List &list);
};

#endif //LAB1_LIST_H
