#include <iostream>

#include "shape.h"

double Rect::get_area() const {
    double area = std::abs((lx - rx) * (ly - ry));
    return area;
}

bool Rect::operator==(const Shape &sh) const
{
    const Rect* rect = dynamic_cast<const Rect*>(&sh);

    if (rect == nullptr)
        return false;

    if ((lx != rect->lx) || (rx != rect->rx))
        return false;

    if ((ly != rect->ly) || (ry != rect->ry))
        return false;

    return Shape::operator==(sh);
}

bool Rect::operator!=(const Shape &sh) const
{
    const Rect* rect = dynamic_cast<const Rect*>(&sh);

    if (rect == nullptr)
        return false;

    if ((lx != rect->lx) || (rx != rect->rx))
        return true;

    if ((ly != rect->ly) || (ry != rect->ry))
        return true;

    return Shape::operator!=(sh);
}

double Circle::get_area() const {
    double area = M_PI * radius * radius;
    return area;
}

bool Circle::operator==(const Shape &sh) const {

    const Circle* circle = dynamic_cast<const Circle*>(&sh);

    if (circle == nullptr)
        return false;

    if ((cx != circle->cx) || (cy != circle->cy) || (radius != circle->radius))
        return false;

    return Shape::operator==(sh);
}

bool Circle::operator!=(const Shape &sh) const {

    const Circle* circle = dynamic_cast<const Circle*>(&sh);

    if (circle == nullptr)
        return false;

    if ((cx != circle->cx) || (cy != circle->cy) || (radius != circle->radius))
        return true;

    return Shape::operator!=(sh);
}