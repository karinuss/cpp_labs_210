#include <typeinfo>
#include <iostream>
#include <string>
#include <cstring>

#include "list.h"

List::Node::Node() {
    pPrev = nullptr;
    pNext = nullptr;
    pShape = nullptr;
}

List::Node::Node(const Node &node) {
    pPrev = nullptr;
    pNext = nullptr;
    pShape = new Shape(*pShape);
}

List::Node& List::Node::operator=(const Node& node) {
    pPrev = std::move(node.pPrev);
    pNext = std::move(node.pNext);
    pShape = std::move(node.pShape);
    return *this;
}

List::Node::Node(Node *pPrev, Shape *pShape) {
    this->pPrev = pPrev;
    if (pPrev != nullptr) {
        this->pNext = pPrev->pNext;
        this->pPrev->pNext = this;
    } else
        this->pNext = nullptr;

    if (this->pNext != nullptr)
        this->pNext->pPrev = this;

    this->pShape = pShape->clone();
}

List::Node::~Node() {
    if (pPrev != nullptr)
        this->pPrev->pNext = this->pNext;

    if (this->pNext != nullptr)
        this->pNext->pPrev = this->pPrev;

    if (this->pShape != nullptr)
        delete this->pShape;
}

List::List() {
    Tail.pPrev = &Head;
    Head.pNext = &Tail;
    m_size = 0;
}

List::~List() {
    Node *p_tmp = nullptr;
    for (Node *p_node = Head.pNext; p_node != &Tail;) {
        p_tmp = p_node;
        p_node = p_node->pNext;
        delete p_tmp;
        m_size--;
    }
}

void List::add(Shape *shape, Node *node) {

    if (shape == nullptr || node == nullptr)
        return;

    Shape *local_shape = nullptr;

    if (typeid(*shape) == typeid(Rect))
        local_shape = new Rect(*(static_cast<Rect *>(shape)));
    else if (typeid(*shape) == typeid(Circle))
        local_shape = new Circle(*(static_cast<Circle *>(shape)));
    else
        local_shape = new Shape(*shape);

    if (local_shape != nullptr) {
        Node *p_node = new Node(node, local_shape);
        m_size++;
    }
}

void List::add_head(Shape *shape) {
    return add(shape, Tail.pPrev);
}

void List::add_tail(Shape *shape) {
    return add(shape, &Head);
}

void List::remove(Shape *shape) {

    if (shape == nullptr)
        return;

    for (Node *p_node = Head.pNext; p_node != &Tail; p_node = p_node->pNext) {
        if (*p_node->get_shape() == *shape) {
            delete p_node;
            m_size--;
            return;
        }
    }
}

void List::swap_nodes(Node *node1, Node *node2) {

    if (node1 == nullptr || node2 == nullptr)
        return;

    Shape *shape_swp = node2->get_shape();
    node2->set_shape(node1->get_shape());
    node1->set_shape(shape_swp);
}

void List::sort_by_area() {

    for (Node *p_node1 = Head.pNext; p_node1 != &Tail; p_node1 = p_node1->pNext)
        for (Node *p_node2 = Head.pNext; p_node2 != Tail.pPrev; p_node2 = p_node2->pNext) {

            if (p_node2->get_shape()->get_area() > p_node1->get_shape()->get_area()) {
                swap_nodes(p_node1, p_node2);
            }
        }

    return;
}

ostream &operator<<(ostream &os, const List &list) {

    unsigned int index = 0;
    for (List::Node *p_node = list.Head.pNext; p_node != &(list.Tail); p_node = p_node->pNext) {
        os << "<List> [" << index++ << "]:";
        os << " <" << typeid(*(p_node->get_shape())).name() << "> | ";

        if (typeid(*(p_node->get_shape())) == typeid(Rect)) {
            const Rect *rect = static_cast<const Rect *>(p_node->get_shape());
            os << *rect << endl;
        } else if (typeid(*(p_node->get_shape())) == typeid(Circle)) {
            const Circle *circle = static_cast<const Circle *>(p_node->get_shape());
            os << *circle << endl;
        } else
            os << *(p_node->get_shape()) << endl;
    }

    return os;
}

istream &operator>>(istream &is, List &list) {

    string s;
    size_t index = 0;
    char s_typeinfo[128] = {0};

    /* Format
       <List> [index]: <typeinfo> | { Shape }
       Example
       <List> [0]: <6Circle> | { color = 3 (cx, cy, radius) = (-1, -10, 10) area 314.159 }
       <List> [1]: <4Rect> | { color = 2 (lx, ly, rx, ry) = (-2, -2, 2, 2) area 16 }
       <List> [2]: <4Rect> | { color = 3 (lx, ly, rx, ry) = (-20, -20, 20, 20) area 1600 }
       <List> [3]: <6Circle> | { color = 0 (cx, cy, radius) = (1, 1, 1) area 3.14159 }
    */
    while (getline(is, s, '|')) // find delimiter
    {
        memset(s_typeinfo, 0, sizeof(s_typeinfo));
        sscanf(s.c_str(), "<List> [%u]: <%[^(<>)]>", &index, s_typeinfo);

        if (!strcmp(s_typeinfo, typeid(Rect).name())) {
            Rect rect;
            is >> rect;
            list.add_head(&rect);
        } else if (!strcmp(s_typeinfo, typeid(Circle).name())) {
            Circle circle;
            is >> circle;
            list.add_head(&circle);
        } else if (!strcmp(s_typeinfo, typeid(Shape).name())) {
            Shape shape;
            is >> shape;
            list.add_head(&shape);
        }

        getline(is, s); // goto end of line
    }

    return is;
}
