#include <iostream>
#include <fstream>

using namespace std;

#include "shape.h"
#include "list.h"

int main() {
    // add shapes
    Rect r1(-2, -2, 2, 2);
    Rect r2(-20, -20, 20, 20);
    Circle c1(10, 0, 0);
    Circle c2(1, 1, 1);
    Circle c3(10, -1, -10);

    r1.set_color(COLOR_BLUE);
    r2.set_color(COLOR_GREEN);
    c1.set_color(COLOR_RED);
    c3.set_color(COLOR_GREEN);

    List list;
    list.add_head(&r2);
    list.add_head(&r1);
    list.add_tail(&c1);
    list.add_tail(&c3);
    list.add_head(&c2);

    cout<<"Print List" << endl << list << endl;
    list.sort_by_area();
    cout<<"Print sorted List" << endl << list << endl;
    list.remove(&c1);
    cout<<"Element "<< c1 << " was removed. Print List again" << endl << list << std::endl;

    // export to file
    const char *fname = "list.txt";
    ofstream fout;
    fout.open(fname);
    fout << list << endl;
    fout.close();

    // read from file
    List list_import;
    ifstream fin;
    fin.open(fname);
    fin >> list_import;
    fin.close();

    // print
    cout<<"Print imported List" << endl << list_import << endl;

    return 0;
}
