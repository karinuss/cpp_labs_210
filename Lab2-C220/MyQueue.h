#ifndef CPP_LABS_210_MYQUEUE_H
#define CPP_LABS_210_MYQUEUE_H

/*
 * Задание 2. Реализуйте шаблонный класс, который является оберткой для очереди с элементами любого типа.
Очередь требуется реализовать посредством динамического массива,
при этом использовать массив как циклический буфер.
Пояснение: так как очередь – это специфическая структура данных, для которой новые данные помещаются в конец,
а «старые» данные изымаются из начала очереди => если последний элемент массива задействован,
то начало скорее всего уже освободилось => «закольцовываем» буфер, продолжая заполнять с нулевого элемента.
Несмотря на указанную специфичность такой структуры данных,
могут возникать ситуации, когда пользователь вызвал push(),
а свободных элементов в очереди не осталось => при необходимости массив следует «расширять».
При реализации нужно обеспечить эффективную работу с динамической памятью=>
· предусмотреть наличие резервных элементов
· память без очевидной необходимости не перераспределять
*/
#include <cstddef>
#include <ostream>
#include <algorithm>

using namespace std;

/* Внимание!
1. Очередь реализуем без использования «сырой памяти»!
 А эффективность достигаем за счет использования move-семантики
2. Очередь выводим на печать с помощью range-base-for
*/

template <typename T> class MyQueue
{
public:
    class MyQueueIterator
    {
    private:
        const MyQueue<T>* m_p_queue;  // указатель на очередь
        size_t m_index = 0;           // текущий индекс итератора

    public:
        MyQueueIterator(const MyQueue<T>* p_queue, size_t index)
        {
            if (p_queue == nullptr)
                throw std::invalid_argument("queue is NULL");

            m_p_queue = p_queue;
            m_index = index % m_p_queue->m_cap;
        }

        bool operator!=(MyQueueIterator const& other) const
        {
            return (m_index != other.m_index) || (m_p_queue != other.m_p_queue);  // сравнить индекс и очередь
        }
        bool operator==(MyQueueIterator const& other) const
        {
            return (m_index == other.m_index) && (m_p_queue == other.m_p_queue); // сравнить индекс и очередь
        }

        MyQueueIterator& operator++()
        {
            m_index = (m_index + 1) % m_p_queue->m_cap;
            return *this;
        }

        const T& operator*() const
        {
            return m_p_queue->m_p[m_index];
        }

        T& operator*()
        {
            return m_p_queue->m_p[m_index];
        }
    };

private:
    T* m_p = nullptr; //указатель на начало динамического массива
    size_t m_size = 0; //актуальное количество элементов в очереди
    size_t m_cap = 0; //емкость (сколько выделено памяти)
    const size_t m_delta = 1; //на сколько увеличиваем емкость при перераспределении памяти
    size_t m_begin = 0; //индекс первого элемента в очереди (это тот элемент, который можно извлечь из очереди с помощью pop())
    size_t m_end = 0; // индекс первого свободного элемента в очереди (это тот элемент, в который можно присвоить новое значение с помощью push())

    void realloc_queue()
    {
        if (m_cap == 0 || m_p == nullptr)
        {
            m_cap = m_delta+1;
            m_p = new T[m_cap];
            m_begin = m_end = 0;
            m_size = 0;
            return;
        }

        size_t new_cap = m_cap + m_delta;
        T* new_p = new T[new_cap];
        size_t new_size = 0;

        for(auto it = begin(); it != end(); ++it)
        {
            new_p[new_size++] = std::move(*it);
        }

        m_begin = 0;
        m_end = m_size = new_size;
        m_cap = new_cap;

        delete[] m_p;
        m_p = new_p;
    }

public:
    MyQueue()
    {
        m_cap = m_delta;
        m_p = new T[m_cap];         // m_p = nullptr, изменить емкость на 1
    }

    MyQueue(size_t reserved) : m_cap(reserved)
    {
        m_p = new T[m_cap+1];
    }

    MyQueue(size_t count, const T& value)
    {
        if (count)
        {
            m_cap = count+1;
            m_p = new T[m_cap];

            for(size_t i = 0; i < count; i++)
            {
                m_p[m_end++] = value;
            }

            m_size = count;
        }
    }

    MyQueue(const initializer_list<T> &list)
    {
        if (list.size())
        {
            m_cap = list.size()+1;
            m_p = new T[m_cap];

            for(const auto& l:list)
            {
                m_p[m_end++] = l;
            }

            m_size = list.size();
        }

    }

    MyQueue(const MyQueue& queue)
    {
        if (queue.m_p)
        {
            m_cap = queue.m_size+1;
            m_p = new T[m_cap];

            for (const auto& q:queue)
            {
                m_p[m_end++] = q;
            }

            m_size = queue.m_size;
        }
    }

    MyQueue& operator=(const MyQueue& queue)  // изменить на перераспределине памяти если не хватает
    {
        if (&queue != this)
        {
            if (queue.m_p == nullptr)
            {
                delete[] m_p;
                m_p == nullptr;
                m_begin = 0;
                m_end = 0;
                m_cap = 0;
                m_size = 0;
            }

            if (queue.m_p)
            {
                if (queue.m_size > m_size)
                {
                    m_cap = queue.m_size + 1;
                    delete[] m_p;
                    m_p = new T[m_cap];
                }

                m_begin = 0;
                m_end = 0;
                m_size = 0;

                for (const auto& q:queue)
                {
                        m_p[m_end++] = q;
                        m_size++;
                }
            }
        }

        return *this;
    }

    MyQueue(MyQueue&& q)
    {
        m_cap = q.m_cap;
        m_p = q.m_p;
        m_begin = q.m_begin;
        m_end = q.m_end;
        m_size = q.m_size;

        q.m_p = nullptr;
        q.m_begin = q.m_end = 0;
        q.m_cap = q.m_size = 0;
    }

    MyQueue& operator=(MyQueue&& q)
    {
        if (&q == this)
            return *this;

        delete[] m_p;

        m_cap = q.m_cap;
        m_p = q.m_p;
        m_begin = q.m_begin;
        m_end = q.m_end;
        m_size = q.m_size;

        q.m_p = nullptr;
        q.m_begin = q.m_end = 0;
        q.m_cap = q.m_size = 0;

        return *this;
    }

    ~MyQueue()
    {
        delete[] m_p;
    }

    MyQueueIterator begin() noexcept { return MyQueueIterator(this, m_begin); }
    const MyQueueIterator begin() const noexcept { return MyQueueIterator(this, m_begin); }
    MyQueueIterator end() noexcept { return MyQueueIterator(this, m_end); }
    const MyQueueIterator end() const noexcept { return MyQueueIterator(this, m_end); }

    void push(const T& val)
    {
        if (m_size+1 >= m_cap)
        {
            realloc_queue();
        }

        m_p[m_end] = val;
        m_size++;
        m_end = (m_end + 1) % m_cap;
    }

    template<typename T1> void push(T1 val)
    {
        if (m_size+1 >= m_cap)
        {
            realloc_queue();
        }

        m_p[m_end] = T(val);
        m_size++;
        m_end = (m_end + 1) % m_cap;
    }

    void push(T&& val)
    {
        if (m_size+1 >= m_cap)
        {
            realloc_queue();
        }

        m_p[m_end] = std::move(val);
        m_size++;
        m_end = (m_end + 1) % m_cap;
    }

    T pop()
    {
        if (m_size == 0) //empty queue
            throw std::invalid_argument("queue is empty)");

        T value = std::move(m_p[m_begin]);
        m_begin = (m_begin + 1) % m_cap;
        m_size--;
        return value;
    }

    size_t size() const { return m_size; }
    size_t capacity() const { return m_cap; }
};

#endif //CPP_LABS_210_MYQUEUE_H
