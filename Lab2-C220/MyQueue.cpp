#include "MyQueue.h"

/*
 * Задание 2. Реализуйте шаблонный класс, который является оберткой для очереди с элементами любого типа.
Очередь требуется реализовать посредством динамического массива,
при этом использовать массив как циклический буфер.
Пояснение: так как очередь – это специфическая структура данных, для которой новые данные помещаются в конец,
а «старые» данные изымаются из начала очереди => если последний элемент массива задействован,
то начало скорее всего уже освободилось => «закольцовываем» буфер, продолжая заполнять с нулевого элемента.
Несмотря на указанную специфичность такой структуры данных,
могут возникать ситуации, когда пользователь вызвал push(),
а свободных элементов в очереди не осталось => при необходимости массив следует «расширять».
При реализации нужно обеспечить эффективную работу с динамической памятью=>
· предусмотреть наличие резервных элементов
· память без очевидной необходимости не перераспределять
*/
#include "MyQueue.h"

#include <string>
#include <iostream>
using namespace std;

MyQueue(const char *) -> MyQueue<string>;

void test_MyQueue()
{
    MyQueue<string> q1{string("AAA"), string ("qwerty") /*<другие_инициализаторы>*/};

    cout << "q1: size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    //использование MyQueue в диапазонном for:
    for (const auto &el : q1)
    {
        std::cout << el << ' ';
    }

    string s("abc");
    q1.push(s);
    cout << "\nq1.push(s): size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    cout << "q1:" ;
    for (const auto &el : q1)
    {
        std::cout << el << ' ';
    }

    q1.push(string("123"));
    cout << "\nq1.push(\"123\"): size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    cout << "q1:" ;
    for (const auto &el : q1)
    {
        std::cout << el << ' ';
    }

    string s1 = q1.pop();
    cout << "\nq1.pop(): size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    cout << "s1 = " << s1 << endl;
    cout << "q1:" ;
    for (const auto &el : q1)
    {
        std::cout << el << ' ';
    }

    q1.push("qqq");
    cout << "\nq1.push(\"qqq\"): size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    cout << "q1:" ;
    for (const auto &el : q1)
    {
        std::cout << el << ' ';
    }

    MyQueue <string> q2 = q1;
    cout << "\nq2 = q1: size = " << q2.size() << " capacity = " << q2.capacity() << endl;
    for (const auto &el : q2)
    {
        std::cout << el << ' ';
    }

    MyQueue <string> q22 = std::move(q1);

    cout << "\nq22 = std::move(q1): size = " << q22.size() << " capacity = " << q22.capacity() << endl;
    for (const auto &el : q22)
    {
      std::cout << el << ' ';
    }

    MyQueue <string> q3{10, string("!")}; //очередь должна содержать 10 элементов со строкой «!»
    cout << "\nq3: size = " << q3.size() << " capacity = " << q3.capacity() << endl;
    for (const auto &el : q3)
    {
      std::cout << el << ' ';
    }

    q1 = q3;

    cout << "\nq1: size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    for (const auto &el : q1)
    {
      std::cout << el << ' ';
    }

    q2 = MyQueue<string>(5, string("?"));
    cout << "\nq2: size = " << q2.size() << " capacity = " << q2.capacity() << endl;
    for (const auto &el : q2)
    {
       std::cout << el << ' ';
    }

    q1 = { string("bbb"), string ("ssss")};
    cout << "\nq1: size = " << q1.size() << " capacity = " << q1.capacity() << endl;
    for (const auto &el : q1)
    {
      std::cout << el << ' ';
    }
}