#ifndef CPP_LABS_210_MYUNIQUEPTR_H
#define CPP_LABS_210_MYUNIQUEPTR_H

template <typename T> class MyUniquePTR
{
private:
    T* m_obj = nullptr;

public:
    MyUniquePTR() = default;

    MyUniquePTR(T* obj)
    {
        m_obj = obj;
    }

    ~MyUniquePTR()
    {
        delete m_obj;
    }

    MyUniquePTR(const MyUniquePTR&) = delete;
    MyUniquePTR& operator=(const MyUniquePTR&) = delete;

    MyUniquePTR(MyUniquePTR&& ptr)
    {
        m_obj = ptr.m_obj;
        ptr.m_obj = nullptr;
    }

    MyUniquePTR& operator=(MyUniquePTR&& ptr)
    {
        if (this != &ptr)
        {
            delete m_obj;
            m_obj = ptr.m_obj;
            ptr.m_obj = nullptr;
        }

        return *this;
    }

    MyUniquePTR& operator=(T&& obj)
    {
       delete m_obj;
       m_obj = new T(obj);
       return *this;
    }

    // operator *
    const T& operator*() const {

        if (m_obj == nullptr)
            throw std::invalid_argument("object is NULL");

        return *m_obj;
    }

    // operator ->
    T* operator->() const {
        return m_obj;
    }

    operator bool() const noexcept
    {
        return (m_obj != nullptr);
    }

    friend std::ostream& operator<<(std::ostream& os, const MyUniquePTR& my_ptr) {
        if (my_ptr.m_obj == nullptr)
            return os << "nullptr";
        return os << *(my_ptr.m_obj);
    }
};

#endif //CPP_LABS_210_MYUNIQUEPTR_H
