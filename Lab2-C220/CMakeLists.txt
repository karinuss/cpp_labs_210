cmake_minimum_required(VERSION 3.16)
project(Lab2_C220)

set(CMAKE_CXX_STANDARD 17)

add_executable(Lab2_C220 lab2_c220.cpp MyVector.cpp MyVector.h MyUniquePtr.cpp MyUniquePtr.h MyQueue.cpp MyQueue.h)