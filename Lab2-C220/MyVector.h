//
// Created by karina on 22.03.2021.
//

#ifndef CPP_LABS_210_MYVECTOR_H
#define CPP_LABS_210_MYVECTOR_H

#include <vector>
#include <ostream>
#include <algorithm>
#include <iterator>

using namespace std;

/*
 * Задание 1. Создайте класс, который должен быть "оберткой" для вектора
    с УНИКАЛЬНЫМИ значениями любого типа в заданном диапазоне.
    Внимание: при инициализации НЕ нужно менять порядок значений,
    заданный пользователем! При наличии повторяющихся значений нужно оставить первое!
    · Для хранения элементов используйте std::vector
    · Реализуйте конструктор, который может принимать любое количество значений (значения могут повторяться)
    · Реализуйте метод добавления любого количества значений (значения могут повторяться)
    · Реализуйте метод удаления любого количества значений (значения могут повторяться)
    · Реализуйте метод сортировки, который будет принимать в качестве параметра признак по возрастанию / по убыванию
    · и другие (полезные на Ваш взгляд) методы
*/

template <typename T> class MyVector
{
private:
    vector<T> m_data;
    const pair<T, T> m_range;

    bool is_exist(const T& value)
    {
        if (find(m_data.cbegin(), m_data.cend(), value) == m_data.end())
            return false;

        return true;
    }

    bool is_range(const T& value)
    {
        if (m_range.first == m_range.second)
            return true;

        if (value >= m_range.first && value <= m_range.second)
            return true;

        return false;
    }

public:
    // конструктор
    MyVector(const initializer_list<T> &list)
    {
        m_data.reserve(list.size());
        copy_if(cbegin(list), cend(list), back_inserter(m_data),
                [this](auto& a) -> bool { return !is_exist(a); });
    }

    MyVector(const initializer_list<T> &list, const pair<T,T> range) : m_range(range)
    {
        m_data.reserve(list.size());
        copy_if(cbegin(list), cend(list), back_inserter(m_data),
                    [this](auto& a) -> bool { return is_range(a) && !is_exist(a); });

    }

    void add(const initializer_list<T> &list)
    {
        m_data.reserve(list.size() + m_data.size());
        for (const auto& l:list)
        {
            if (!is_range(l))
                continue;

            if (!is_exist(l))
                m_data.push_back(l);
        }

    }

    void remove(const initializer_list<T> &list)
    {
        for (const auto& l:list)
        {
            if (!is_range(l))
                continue;

            m_data.erase(remove_if(m_data.begin(), m_data.end(),
                                   [&l](const auto& v) -> bool { return v == l; }), m_data.end());
        }
    }

    // sort c bool asc/desc
    void sort(bool is_ascending)
    {
        std::sort(m_data.begin(), m_data.end(), [is_ascending](const T& value1, const T& value2) -> bool {
                                                                if (is_ascending)
                                                                    return (value1 < value2);
                                                                else
                                                                    return (value2 < value1);
                                                                });
    }

    friend std::ostream& operator<<(std::ostream& os, const MyVector& my_vector) {
        os << "range { " << my_vector.m_range.first << ", " << my_vector.m_range.second << " }";

        for (const auto &v : my_vector.m_data)
        {
            os << " " << v ;
        }

        return os;
    }
};

#endif //CPP_LABS_210_MYVECTOR_H
