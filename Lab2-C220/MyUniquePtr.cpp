
#include <string>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

#include "MyUniquePtr.h"

using namespace std;
/*
* Задание 3. Реализуйте шаблон класса MyUniquePTR,
который является оберткой для указателя на объект любого типа.
Задача – класс должен обеспечивать единоличное владение динамически создаваемым объектом.
Проверить функционирование шаблона на примере MyString.
*/

MyUniquePTR(const char *) -> MyUniquePTR<string>;

void test_MyUniquePTR()
{
   MyUniquePTR<string> p1(new string("abc"));
   std::cout << "p1 = " << p1 << endl;
   p1 = "qwerty";
   std::cout << "p1 = \"qwerty\": p1 = " << p1 << endl;

   string s2 = *p1;
   cout << "s2 = *p1: s2 = " << s2 << endl;

  MyUniquePTR<string> p2 = std::move(p1); //здесь компилятор должен выдавать ошибку => Исправьте !

  cout << "p2 = std::move(p1): p1 = " << p1 << " p2 = " << p2 << endl;

  if(!p1)
  {
       std::cout << "p1 -> No object!" << endl;
  } //а это должно работать

   MyUniquePTR<string> p3(new string("vvv"));
   p3 = std::move(p2); //и здесь компилятор должен выдавать ошибку
   cout << "p3 = std::move(p2): p2 = " << p2 << " p3 = " << p3 << endl;

   vector <MyUniquePTR<string>> v; //как проинициализировать???
   v.push_back(MyUniquePTR<string>(new string("aaa")));
   v.push_back(MyUniquePTR<string>(new string("hhh")));
   v.push_back(MyUniquePTR<string>(new string("zzz")));

   cout << "vector v: ";
    for(const auto& iv:v)
    {
        cout << iv << " ";
    }

   list <MyUniquePTR<string>> l(make_move_iterator(v.begin()), make_move_iterator(v.end())); //как скопировать из v в l ???

   cout << "\nlist l: ";
   for(const auto& il:l)
   {
        cout << il << " ";
   }

   cout << "\nmoved v: ";
   for(const auto& iv:v)
   {
       cout << iv << " ";
   }
}