//
// Created by karina on 22.03.2021.
//

#include <iostream>
#include "MyVector.h"

using namespace std;

void test_MyVector()
{

/*
 * Задание 1. Создайте класс, который должен быть "оберткой" для вектора
    с УНИКАЛЬНЫМИ значениями любого типа в заданном диапазоне.
    Внимание: при инициализации НЕ нужно менять порядок значений,
    заданный пользователем! При наличии повторяющихся значений нужно оставить первое!
    · Для хранения элементов используйте std::vector
    · Реализуйте конструктор, который может принимать любое количество значений (значения могут повторяться)
    · Реализуйте метод добавления любого количества значений (значения могут повторяться)
    · Реализуйте метод удаления любого количества значений (значения могут повторяться)
    · Реализуйте метод сортировки, который будет принимать в качестве параметра признак по возрастанию / по убыванию
    · и другие (полезные на Ваш взгляд) методы
*/

    //MyVector<int> my_v_int = {{ 1, 7, 6, 9, 25, 9, 10 }, pair<int, int>(5,10)};
    MyVector<int> my_v_int = { 1, 7, 6, 9, 25, 9, 10 };
    cout << "my_v_int created: " << my_v_int << std::endl;

    my_v_int.add({ 5, 99, 7, 3 ,7, 8, 9, 10 });
    cout << "my_v_int add: " << my_v_int << std::endl;

    my_v_int.remove({ 99, 7, 3 ,7 });
    cout << "my_v_int remove: " << my_v_int << std::endl;

    my_v_int.sort(true);
    cout << "my_v_int sort in ascending order: " << my_v_int << std::endl;
    my_v_int.sort(false);
    cout << "my_v_int sort in descending order: " << my_v_int << std::endl;

    cout << "\n\n";
    MyVector<string> my_v_str = {{ "a", "h", "hy", "uy", "gg", "zz", "df", "h" }, pair<string, string>("c","y")};
    cout << "my_v_str created: " << my_v_str << std::endl;

    my_v_str.add({ "b", "c", "e", "z", "x", "f", "e" });
    cout << "my_v_str add: " << my_v_str << std::endl;

    my_v_str.remove({ "b", "c", "e", "z" });
    cout << "my_v_str remove: " << my_v_str << std::endl;

    my_v_str.sort(true);
    cout << "my_v_str sort in ascending order: " << my_v_str << std::endl;
    my_v_str.sort(false);
    cout << "my_v_str sort in descending order: " << my_v_str << std::endl;
}