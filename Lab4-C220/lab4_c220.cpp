#include <cstdio>
#include <fstream>

#include <iostream>
#include <memory>
#include <vector>
#include <set>
#include <algorithm>
#include <string>
#include <list>

void human();

using namespace std;

template <typename T> void print_values(T& value, const char* name)
{
    cout << name << ": ";

    for (const auto& v:value)
    {
        if constexpr (is_pointer_v<decay_t<decltype(v)>>)
            if (v == nullptr)
                cout << " " << nullptr;
            else
                cout << " " << *v;
        else
        {
            if constexpr (is_same_v<decay_t<decltype(v)>, unique_ptr<string>>)
                if (v == nullptr)
                    cout << " " << nullptr;
                else
                    cout << " " << *v;
            else
            if constexpr (is_same_v<decay_t<decltype(v)>, shared_ptr<string>>)
                if (v == nullptr)
                    cout << " " << nullptr;
                else
                    cout << " " << *v;
            else
                cout << " " << v;
        }
    }

    cout << endl;
}

template <typename T> class MyDeleter {
    size_t m_count;

public:
    MyDeleter(size_t count) : m_count(count) {};
    void operator()(T* p)
    {
       // cout << "m_count = " << m_count << " ";
        for (size_t i = 0; i < m_count; i++)
        {
            //cout << " " << *p[i];
            delete p[i];
        }
    }
};

int main()
{

    //////////////////////////////////////////////////////////////////////////////////////////////
    //Задание 1. unique_ptr
    {
        cout << "\n\n*** Задание 1 ***\n\n";

        //1.а - обеспечьте корректное выполнение фрагмента
        {
            cout << "\n\n*** Задание 1a ***\n\n";
            std::vector<std::string*> v1 = {new std::string("aa"), new std::string("bb"), new std::string("cc")};
            //Распечатайте все строки
            print_values(v1, "v1");
            for_each(begin(v1), end(v1), [](string* obj)
            { delete obj; });
            //???
        } //???

        //1.b - модифицируйте задание 1.а:
        //обеспечьте посредством std::unique_ptr:
        //эффективное заполнение (вспомните про разные способы формирования std::unique_ptr),
        //безопасное хранение указателей на динамически создаваемые объекты std::string,
        //манипулирование,
        //и освобождение ресурсов
        //

        {
            cout << "\n\n*** Задание 1b ***\n\n";
            std::vector<unique_ptr<string>> v2;
            v2.reserve(3);
            v2.push_back(make_unique<string>("aa"));
            v2.push_back(make_unique<string>("bb"));
            v2.push_back(make_unique<string>("cc"));
            //Распечатайте все строки
            print_values(v2, "v2");

            //??? Уничтожение динамически созданных объектов?
            //for_each(begin(v2), end(v2), [](unique_ptr<string> obj){ obj.reset(); });

        } //???

        {//1.c - дополните задание 1.b добавьте возможность изменять хранящиеся строки
            //следующим образом (например, добавить указанный суффикс: "AAA" -> "AAA_1")
            cout << "\n\n*** Задание 1c ***\n\n";
            std::vector<unique_ptr<string>> v3;
            v3.reserve(3);
            v3.push_back(make_unique<string>("aa"));
            v3.push_back(make_unique<string>("bb"));
            v3.push_back(make_unique<string>("cc"));
            //Распечатайте все строки
            print_values(v3, "v3");

            string postfix = "_1";
            for (auto &v:v3)
            {
                *v += postfix;
            }

            print_values(v3, "v3_postfix");
        }

        {//1.d - динамический массив объектов
            cout << "\n\n*** Задание 1d ***\n\n";
            //Создайте unique_ptr, который является оберткой для динамического массива
            //с элементами std::string
            //С помощью unique_ptr::operator[] заполните обернутый массив значениями
            //Когда происходит освобождения памяти? -> При } - окончание области видимости
            unique_ptr<string[]> p_str = make_unique<string[]>(10);
            p_str[0] = "aa";
            p_str[1] = "bb";
            p_str[2] = "cc";
        }

        {//1.e - массивы динамических объектов и пользовательская delete-функция (функтор)
            //Задан стековый массив указателей на динамически созданные объекты
            //Создайте unique_ptr для такого массива
            //Реализуйте пользовательскую delete-функцию (функтор) для корректного
            //освобождения памяти
            cout << "\n\n*** Задание 1e ***\n\n";
            std::string* arStrPtr[] = {new std::string("aa"), new std::string("bb"), new std::string("cc")};
            print_values(arStrPtr, "arStrPtr");

            auto deleter = [count = size(arStrPtr)](string** s) { for (size_t i = 0; i < count; i++) delete s[i]; };
            //unique_ptr<string*, MyDeleter<string*>>
              //      p_str2(move(arStrPtr), MyDeleter<string*>(size(arStrPtr)));
            unique_ptr<string*, decltype(deleter)> p_str2(arStrPtr, deleter);
        }

        {//1.f Создайте и заполните вектор, содержащий unique_ptr для указателей на std::string
            //Посредством алгоритмя copy() скопируйте элементы вектора в пустой список с элементами
            //того же типа
            //Подсказка: перемещающие итераторы и шаблон std::make_move_iterator
            cout << "\n\n*** Задание 1f ***\n\n";
            std::vector<unique_ptr<string>> v4;
            v4.reserve(3);
            v4.push_back(make_unique<string>("aa"));
            v4.push_back(make_unique<string>("bb"));
            v4.push_back(make_unique<string>("cc"));
            print_values(v4, "v4");

            list<unique_ptr<string>> v4_copy;
            copy(make_move_iterator(v4.begin()), make_move_iterator(v4.end()),
                        back_inserter(v4_copy));
                // insert_iterator<list<unique_ptr<string>>>(v4_copy, v4_copy.begin()));
            print_values(v4_copy, "v4_copy");
            print_values(v4, "v4");
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    //Задание 2.shared_ptr + пользовательская delete-функция
    cout << "\n\n*** Задание 2 ***\n\n";

    //Реализовать возможность записи в файл данных (строчек) из разных источников
    //(для упрощения пусть источниками являются два массива)
    //Так как все "писатели" будут по очереди записывать свои данные в один и тот же файл,
    //логично предоставить им возможность пользоваться одним и тем же указателем FILE* =>
    //безопасной оберткой для такого указателя является shared_ptr
    //а. Первый владелец должен открыть/создать файл для записи
    //б. Все остальные писатели должны присоединиться к использованию
    //в. Последний владелец указателя должен закрыть файл

    //Подсказка: имитировать порядок записи можно с помощью функции rand()

    {
        //"писатели":
        //Создать writer1, writer2
        //например, источники данных:
        //char ar1[] = "Writer1";
        //char ar2[] = "Writer2";
        const char* ar1[] = {"aa", "bb", "cc", "dd", "ee", "ff", "pp"};
        const char* ar2[] = {"jj", "ll", "mm", "nn", "oo", "zz"};

        const char* fname = "lab4-C220-test.txt";
        shared_ptr<FILE> writer1(fopen(fname, "w+"), &fclose);
        cout << "writer1.use_count(): " << writer1.use_count() << endl;
        shared_ptr<FILE> writer2 = writer1;
        cout << "writer2.use_count(): " << writer2.use_count() << endl;

        //заданное число итераций случайным образом позволяем одному из "писателей" записать в файл
        //свою строчку
        //Подсказка: строчки удобно записывать в файл посредством функции fputs()

        for (size_t i = 0, j = 0, k = 0; (j < size(ar1)) && (k < size(ar2)) && (i < size(ar1) + size(ar2)); i++)
        {
            if (!i % 2)
            {
                fputs(ar1[j++], writer1.get());
                fputs("\n", writer1.get());
            }
            else
            {
                fputs(ar2[k++], writer2.get());
                fputs("\n", writer2.get());
            }

        }

    }//закрытие файла???
/***************************************************************/
//Задание 3.
    {
        cout << "\n\n*** Задание 3 ***\n\n";

        //Дан массив элементов типа string
        std::string strings[] = {"zzz", "abc", "123", "qwerty", "#$%", "55"};
        //До завершения фрагмента строки должны существовать в единственном экземпляре.
        //Требуется обеспечить манипулирование строками а) без копирования и б) без изменения порядка
        //элементов в массиве!

        //В std::set "складываем" по алфавиту обертки для строк, которые содержат только буквы
        //auto comp = [](shared_ptr<string> s1, shared_ptr<string> s2) -> bool
        //{ return std::tolower((*s1)[0]) < std::tolower((*s2)[0]); };

        auto comp = [](const auto& s1, const auto& s2) -> bool
        { return std::tolower((*s1)[0]) < std::tolower((*s2)[0]); };

        std::set<shared_ptr<string>, decltype(comp)> set_alpha(comp); // добавить компаратор

        auto is_not_alpha = [](const string &s) -> bool
        {
            return find_if(begin(s), end(s), [](char c)
            { return !std::isalpha(c); }) != end(s);
        };

        for (const auto &s:strings)
        {
            if (is_not_alpha(s))
                continue;

            set_alpha.insert(make_shared<string>(s));
        }

        print_values(set_alpha, "set_alpha");

        /******************************************************************************************/

        //В std::vector "складываем" обертки для строк, которые содержат только цифры
        //Выводим на экран
        //Находим сумму

        std::vector<std::shared_ptr<std::string>> v_digit;
        v_digit.reserve(size(strings));

        auto is_not_digit = [](const string &s) -> bool
        {
            return find_if(begin(s), end(s), [](char c)
            { return !std::isdigit(c); }) != end(s);
        };

        for (const auto &s:strings)
        {
            if (is_not_digit(s))
                continue;

            v_digit.push_back(make_shared<string>(s));
        }

        print_values(v_digit, "v_digit");

        unsigned int sum = 0;
        for (const auto &s:v_digit)
        {
            sum += stoi(*s);
        }

        cout << "sum v_digit = " << sum << endl;
        /******************************************************************************************/
        //сюда "складываем" обертки для строк, которые не содержат ни символов букв, ни символов цифр
        //и просто выводим
        std::vector<std::shared_ptr<std::string>> v_other;
        v_other.reserve(size(strings));

        for (const auto &s:strings)
        {
            if (is_not_digit(s) && is_not_alpha(s))
                v_other.push_back(make_shared<string>(s));
        }

        print_values(v_other, "v_other");
    }

/******************************************************************************************/
//Задание 4.
    {
        cout << "\n\n*** Задание 4 ***\n\n";

        //Дано:
        std::string ar[] = {"my", "Hello", "World"};
        std::vector<std::shared_ptr<std::string>> v = {std::make_shared<std::string>("good"),
                                                       std::make_shared<std::string>("bye")};
        //а) Требуется добавить в вектор обертки для элементов массива, НЕ копируя элементы массива!
        //б) Отсортировать вектор по алфавиту и вывести на экран
        //в) Обеспечить корректное освобождение памяти

        print_values(v, "v");

        v.reserve(v.size()+size(ar));

        for (auto& a:ar)
        {
            v.push_back(shared_ptr<string>(&a, [](string *s) {}));
        } /*
        transform(begin(ar), end(ar),
                  insert_iterator<vector<shared_ptr<string>>>(v, v.begin()),
                  [](const string &v)
                  { return make_shared<string>(v); });*/

        print_values(ar, "ar");
        print_values(v, "v");


        std::sort(v.begin(), v.end(), [](shared_ptr<std::string> s1, shared_ptr<std::string> s2) -> bool
        { return std::tolower((*s1)[0]) < std::tolower((*s2)[0]); });
        print_values(v, "v_sorted");
    }
    /***************************************************************/
    human();
}


class Human
{
    bool m_is_life = true;
    shared_ptr<Human> m_mother, m_father;
    vector<weak_ptr<Human>> m_children;
    string m_name = "noname";
public:
    Human() = default;
    Human(const string name) : m_name(name) {}
    Human(const string name, bool is_life) : m_name(name), m_is_life(is_life) {}

    Human(const Human& human) = delete;
    Human& operator=(const Human& human) = delete;
    Human(Human&& human) = delete;
    Human& operator=(Human&& human) = delete;

    static void print_children(std::ostream& os, const Human& human)
    {
        for (const auto& child:human.m_children)
        {
            shared_ptr<Human> h = child.lock();
            if (h)
            {
                os << "child name: " << h->m_name << " is_life: " << h->m_is_life << endl;
                print_children(os, *h);
            }
        }
    }

    static void print_parents(std::ostream& os, const Human& human)
    {
        os << "name: " << human.m_name << " is_life: " << human.m_is_life << endl;
        os << "{ Mother of " << human.m_name << ": ";
        if (human.m_mother == nullptr)
            os << " no mother ";
        else
            print_parents(os, *(human.m_mother));

        os << "  Father of "<< human.m_name <<": ";
        if (human.m_father == nullptr)
            os << " no father ";
        else
            print_parents(os, *(human.m_father));
        os << " } " << endl;
    }

    friend std::ostream& operator<<(std::ostream& os, const Human& human) {
        os << "name: " << human.m_name << " is_life: " << human.m_is_life << endl;
        os << "children[count = " << human.m_children.size() << "]:" << endl;
        print_children(os, human);
        os << "parents:" << endl;
        print_parents(os, human);
    }

    void add_child(const shared_ptr<Human>& child)
    {
        m_children.push_back(child);
    }

    static shared_ptr<Human> child(const string& name, shared_ptr<Human> mother, shared_ptr<Human> father)
    {
        shared_ptr<Human> child(new Human(name));
        child->m_mother = mother;
        child->m_father = father;
        mother->add_child(child);
        father->add_child(child);

        return child;
    }
};

void human()
{
    //Задание 5. shared_ptr и weak_ptr
    cout << "\n\n*** Задание 5 ***\n\n";

    //Создаем генеалогическое дерево посредством класса human. В классе хранятся:
    //имя - string
    //возможно признак: жив или уже нет...
    //родители - shared_ptr (родители не всегда известны...)
    //дети - контейнер из weak_ptr (чтобы избежать циклических зависимостей)

    //Методы класса human:
    //конструктор - для инициализации имени и признака
    //конструктор копирования, оператор присваивания, move ???
    //статический метод child() -
    //				должен создать создать и вернуть обертку для родившегося человека
    //				+ сформировать все связи ребенка с родителями и наоборот



    //Ввести возможность распечатать генеалогическое дерево для указанного индивидума

        //История должна с кого-то начинаться => "Жили-были дед да баба, например, Адам и Ева"
        //(то есть на самом деле два деда и две бабы):


        std::shared_ptr<Human> grandM1(new Human("Eva1"));
        std::shared_ptr<Human> grandF1(new Human("Adam1", false));
       // cout << *grandM1;
      //  cout << *grandF1;

        std::shared_ptr<Human> grandM2(new Human("Eva2"));
        std::shared_ptr<Human> grandF2(new Human("Adam2", false));
      //  cout << *grandM2;
      //  cout << *grandF2;
        //...

        //у них появились дети - child():
        std::shared_ptr<Human> mother11 = Human::child("Masha1", grandM1, grandF1);
        std::shared_ptr<Human> father22 = Human::child("Petya1", grandM2, grandF2);

       // cout << *mother11;
       // cout << *father22;

        //а у детей в свою очередь свои дети:
        std::shared_ptr<Human> child1 = Human::child("Sasha", mother11, father22);
        std::shared_ptr<Human> child2 = Human::child("Misha", mother11, father22);
       // cout << *child1;
        //...
        cout << *mother11;
}