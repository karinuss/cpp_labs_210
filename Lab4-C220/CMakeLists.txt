cmake_minimum_required(VERSION 3.16)
project(Lab4_C220)

set(CMAKE_CXX_STANDARD 17)

add_executable(Lab4_C220 lab4_c220.cpp)