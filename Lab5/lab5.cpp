// Iter_Alg.cpp : Defines the entry point for the console application.
//
//Итераторы. Стандартные алгоритмы. Предикаты.

#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <algorithm>
#include <cmath>
#include <cstring>

using namespace std;

class Point {
private:
    float x, y;

public:
    Point() { this->x = 0; this->y = 0; }
    Point(float x, float y) { this->x = x; this->y = y; }

    float get_distance() const {
        return sqrt(this->x * this->x + this->y * this->y);
    }

    virtual bool operator<(const Point &point) const {  // for container 'set'
        float d_point = point.get_distance();
        float d_this = this->get_distance();
        return (d_point > d_this);
    }

    // for std::find()
    virtual bool operator==(const Point &point) const { return (this->x == point.x && this->y == point.y); }

    friend std::ostream& operator<<(std::ostream& os, const Point& point) {
        os << "{ " << point.x << ", " << point.y << " [d = " << point.get_distance() << "] }";
        return os;
    }

    static bool sort_x_order(const Point &point1, const Point &point2)
    {
        //if ((point1.x > point2.x) && (point1.y > point2.y))
        return (point2.x > point1.x);
    }

    static Point get_center_point(const Point &point1, const Point &point2)
    {
        return Point((point1.x+point2.x)/2, (point1.y+point2.y)/2);
    }

    friend class FindIfPoint;
};

class FindIfPoint
{
private:
    Point m_value_lo;
    Point m_value_up;
public:
    FindIfPoint(const Point value_lo, const Point value_up) : m_value_lo(value_lo), m_value_up(value_up) {};

    bool operator()(const Point& value)
    {
        if (value.x > m_value_up.x || value.x < m_value_lo.x)
            return false;

        if (value.y > m_value_up.y || value.y < m_value_lo.y)
            return false;

        return true;
    }
};

class Rect
{
private:
    Point m_l_point;
    Point m_r_point;

public:
    Rect(const Point l_point, const Point r_point) : m_l_point(l_point), m_r_point(r_point) { };

    Point get_center_point() const {
        return Point::get_center_point(m_l_point, m_r_point);
    }

    bool operator<(const Rect &rect) const // для шаблона template <typename T> struct SortValueDescOrder
    {
        Point center_this_rect = this->get_center_point();
        Point center_rect = rect.get_center_point();

        return center_this_rect < center_rect;
    }

    friend ostream &operator<<(ostream &os, const Rect &rect) {
        os << "{ lpoint " << rect.m_l_point << " rpoint " << rect.m_r_point;
        os << " center " << Point::get_center_point(rect.m_l_point, rect.m_r_point) <<" }" << endl;
        return os;
    }
};

template <typename T, typename C>  std::ostream& operator<< (std::ostream& os, set<T, C> s)
{
    for(typename set<T,C>::const_iterator iter = s.cbegin(); iter != s.cend(); ++iter)
        os << *iter << " ";
    return os;
}

template <typename T>  std::ostream& operator<< (std::ostream& os, vector<T> v)
{
    for(typename vector<T>::const_iterator iter = v.cbegin(); iter != v.cend(); ++iter)
        os << *iter << " ";
    return os;
}

template <typename T>  std::ostream& operator<< (std::ostream& os, list<T> l)
{
    for(typename list<T>::const_iterator iter = l.cbegin(); iter != l.cend(); ++iter)
        os << *iter << " ";
    return os;
}

template <typename T> void print_info(const T &obj, const string name)
{
    cout << "\n" << name << ":" << " size " << obj.size();
    cout << "\n" << name << ": ";
    cout << obj << endl;
}

template <typename In, typename Out> void print_values(const In start, const In end, Out print)
{
    for(In iter = start; iter != end; ++iter)
    {
        *print = *iter;
        ++print;
    }
}

static void test_iterators1() {
    ///////////////////////////////////////////////////////////////////

    //Задание 1. Итераторы

    //Реверсивные итераторы. Сформируйте set<Point>. Подумайте, что
    //нужно перегрузить в классе Point. Создайте вектор, элементы которого
    //являются копиями элементов set, но упорядочены по убыванию

    set<Point> sPoint1 = { Point(19., 67.), Point(5.5, 78.7),
                           Point(18., 3.35), Point(8.3, 30.0), Point(8.0, 0.5) };

    print_info(sPoint1, "Creating sPoint1");

    vector<Point> vPoint1(sPoint1.crbegin(), sPoint1.crend());
    print_info(sPoint1, "Creating vPoint1 - reverse sPoint1");

    //Потоковые итераторы. С помощью ostream_iterator выведите содержимое
    //vector и set из предыдущего задания на экран.
    cout << "Ostream iterator sPoint1: ";
    ostream_iterator<Point> os_point1(cout, " # ");
    print_values(sPoint1.cbegin(), sPoint1.cend(), os_point1);
    cout << "\nOstream iterator vPoint1: ";
    print_values(vPoint1.cbegin(), vPoint1.cend(), os_point1);

    //Итераторы вставки. С помощью возвращаемых функциями:
    //back_inserter()
    //front_inserter()
    //inserter()
    //итераторов вставки добавьте элементы в любой из созданных контейнеров. Подумайте:
    //какие из итераторов вставки можно использовать с каждым контейнером.
    list<Point> lPoint1(vPoint1.cbegin(), vPoint1.cend());
    print_info(lPoint1, "list lPoint1: created from vector vPoint1");
    Point ins_point(55, 55);

    //back_insert_iterator<set<Point>> s_back_it = back_inserter(sPoint1); // не работает
    //s_back_it = ins_point;
    //print_info(sPoint1, "set sPoint1: back_inserter Point(55,55)");

    back_insert_iterator<vector<Point>> v_back_it = back_inserter(vPoint1);
    v_back_it = ins_point;
    print_info(vPoint1, "vector vPoint1: back_inserter Point(55,55)");

    back_insert_iterator<list<Point>> l_back_it = back_inserter(lPoint1);
    l_back_it = ins_point;
    print_info(lPoint1, "list lPoint1: back_inserter Point(55,55)");

    ins_point = Point(77, 77);
    //front_insert_iterator<set<Point>> s_front_it = front_inserter(sPoint1); // не работает
    //s_front_it = ins_point;
    //print_info(sPoint1, "set sPoint1: front_inserter Point(77,77)");

    //front_insert_iterator<vector<Point>> v_front_it = front_inserter(vPoint1); // не работает
    //v_front_it = ins_point;
    //print_info(vPoint1, "vector vPoint1: front_inserter Point(77,77)");

    front_insert_iterator<list<Point>> l_front_it = front_inserter(lPoint1); // не работает
    l_front_it = ins_point;
    print_info(lPoint1, "list lPoint1: front_inserter Point(77,77)");

    ins_point = Point(99,99);
    insert_iterator<set<Point>> s_ins_it = inserter(sPoint1, sPoint1.begin());
    s_ins_it = ins_point;
    print_info(sPoint1, "set sPoint1: inserter Point(99,99) at begin");

    vector<Point>::iterator v_it = vPoint1.begin();
    std::advance(v_it, 3);
    insert_iterator<vector<Point>> v_ins_it = inserter(vPoint1, v_it);
    v_ins_it = ins_point;
    print_info(vPoint1, "vector vPoint1: inserter Point(99,99) at begin+3");

    insert_iterator<list<Point>> l_ins_it = inserter(lPoint1, ++lPoint1.begin());
    l_ins_it = ins_point;
    print_info(lPoint1, "list lPoint1: inserter Point(99,99) at ++begin");
}

template <typename T, typename C> std::ostream& operator<< (std::ostream& os, const pair<T, C> &p)
{
    os << p.first << " " << p.second << endl;
    return os;
}

template <typename T> void print_for_each_value(const T& value)
{
    cout << " " << value;
}

template <typename T> class ChangeValue
{
private:
    T value;
public:
    ChangeValue(const T new_value) : value(new_value) {};

    void operator()(T& old_value)
    {
      old_value = value;
    }
};

template <typename T> struct SortValueDescOrder
{
    bool operator()(T& value1, T& value2)
    {
        return (value1 < value2);
    }
};

static void test_algo2()
{
    ///////////////////////////////////////////////////////////////////

    //Задание 2. Обобщенные алгоритмы (заголовочный файл <algorithm>). Предикаты.

    // алгоритм for_each() - вызов заданной функции для каждого элемента любой последовательности
    //(массив, vector, list...)
    //С помощью алгоритма for_each в любой последовательности с элементами любого типа
    //распечатайте значения элементов
    //Подсказка : неплохо вызываемую функцию определить как шаблон
    Point pMas[] = { Point(19., 67.), Point(5.5, 78.7),
                     Point(-18., 3.35), Point(8.3, 30.0), Point(-8.0, -0.5) };
    set<Point> sPoint2(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]));
    vector<Point> vPoint2(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]));
    list<Point> lPoint2(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]));

    cout << "\nCreating array pMas, for_each print:" << endl;
    for_each(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]), print_for_each_value<Point>);
    cout << "\nCreating set sPoint2, for_each print:" << endl;
    for_each(sPoint2.begin(), sPoint2.end(), print_for_each_value<Point>);
    cout << "\nCreating vector vPoint2, for_each print:" << endl;
    for_each(vPoint2.begin(), vPoint2.end(), print_for_each_value<Point>);
    cout << "\nCreating list lPoint2, for_each print:" << endl;
    for_each(lPoint2.begin(), lPoint2.end(), print_for_each_value<Point>);

    //С помощью алгоритма for_each в любой последовательности с элементами типа Point
    //измените "координаты" на указанное значение (такой предикат тоже стоит реализовать
    //как шаблон) и выведите результат с помощью предыдущего предиката
    cout << "\n";
    cout << "\nChange vector vPoint2 to Point(99,99), for_each print:" << endl;
    for_each(vPoint2.begin(), vPoint2.end(), ChangeValue<Point>(Point(99,99)));
    for_each(vPoint2.begin(), vPoint2.end(), print_for_each_value<Point>);

    for_each(++lPoint2.begin(), --lPoint2.end(), ChangeValue<Point>(Point(77,77)));
    cout << "\nChange list lPoint2 from 1 to 3 elems to Point(77,77), for_each print:" << endl;
    for_each(lPoint2.begin(), lPoint2.end(), print_for_each_value<Point>);

    //С помощью алгоритма find() найдите в любой последовательности элементов Point
    //все итераторы на элемент Point с указанным значением.
    cout << "\n\nFind Point(77,77):";
    list<Point>::iterator find_it = lPoint2.begin();
    while(1)
    {
        find_it = find(find_it, lPoint2.end(), Point(77,77));

        if (find_it == lPoint2.end())
            break;

        cout << "\nfind_it = " << *find_it;
        ++find_it;
    }

    //С помощью алгоритма sort() отсортируйте любую последовательность элементов Point.
    ////По умолчанию алгоритм сортирует последовательность по возрастанию.
    //Что должно быть определено в классе Point?
    // Замечание: обобщенный алгоритм sort не работает со списком, так как
    //это было бы не эффективно => для списка сортировка реализована методом класса!!!
    list<Point> lPoint3(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]));
    cout << "\n\nCreating list lPoint3, for_each print:" << endl;
    for_each(lPoint3.begin(), lPoint3.end(), print_for_each_value<Point>);

    lPoint3.sort(Point::sort_x_order);

    cout << "\n\nSort list lPoint3 by X coord(ascending), for_each print:" << endl;
    for_each(lPoint3.begin(), lPoint3.end(), print_for_each_value<Point>);

    vector<Point> vPoint3(pMas, pMas+sizeof(pMas)/sizeof(pMas[0]));
    cout << "\n\nCreating vector vPoint3, for_each print:" << endl;
    for_each(vPoint3.begin(), vPoint3.end(), print_for_each_value<Point>);

    sort(vPoint3.begin(), vPoint3.end(), SortValueDescOrder<Point>());

    cout << "\n\nSort vector vPoint2 by default '<' (by distance from 0), for_each print:" << endl;
    for_each(vPoint3.begin(), vPoint3.end(), print_for_each_value<Point>);

    //С помощью алгоритма find_if() найдите в любой последовательности элементов Point
    //итератор на элемент Point, удовлетворяющий условию: координаты x и y лежат в промежутке
    //[-n, +m].
    Point check_point1(-10, -10), check_point2(7, 80);
    cout << "\n\nFind Points from (-10, -10) to (7, 80):";
    list<Point>::iterator find_if_it = lPoint3.begin();
    while(1)
    {
        find_if_it = find_if(find_if_it, lPoint3.end(), FindIfPoint(check_point1, check_point2));

        if (find_if_it == lPoint3.end())
            break;

        cout << "\nfind_it = " << *find_if_it;
        ++find_if_it;
    }

    //С помощью алгоритма sort() отсортируйте любую последовательность элементов Rect,
    //располагая прямоугольники по удалению центра от начала координат.
    vector<Rect> vRects;
    vRects.push_back(Rect(Point(-10, 1), Point(-9, 2)));
    vRects.push_back(Rect(Point(5, 5), Point(3, 4)));
    vRects.push_back(Rect(Point(-1, 10), Point(-3, 1)));
    vRects.push_back(Rect(Point(50, -4), Point(9, 12)));
    vRects.push_back(Rect(Point(-8, -1), Point(-6, -3)));

    print_info(vRects, "Created vRects");

    struct comp_rects {
        bool operator() (const Rect& rect1, const Rect& rect2) const
        {
            Point center_rect1 = rect1.get_center_point();
            Point center_rect2 = rect2.get_center_point();

            return center_rect1.get_distance() < center_rect2.get_distance();
        }
    };

    //sort(vRects.begin(), vRects.end(), SortValueDescOrder<Rect>());
    sort(vRects.begin(), vRects.end(), comp_rects());
    cout << "\n\nSort vector vPoint2 by default '<' (by distance from 0):" << endl;
    for_each(vRects.begin(), vRects.end(), print_for_each_value<Rect>);
}

static void test_algo3()
{

    {//transform
        //Напишите функцию, которая с помощью алгоритма transform переводит
        //содержимое объекта string в нижний регистр.
        //Подсказка: класс string - это "почти" контейнер, поэтому для него
        // определены методы begin() и end()
        struct transform_to_lower {
            char operator() (char c)
            {
                 return std::tolower(c);
            }

            string operator() (string s)
            {
                std::transform(s.begin(), s.end(), s.begin(), transform_to_lower());
                return s;
            }
        };

        string s1("QWERTY"), s2("aSdFGh"), s1_out("");

        transform(s1.begin(), s1.end(), back_insert_iterator<string>(s1_out), transform_to_lower());
       // transform(s1.begin(), s1.end(), back_insert_iterator<string>(s1_out), [](char c) { return c = std::tolower(c); });
        cout << "\n\ns1 = " << s1 << " s1_out = " << s1_out << endl;

        //Заполните list объектами string. С помощью алгоритма transform сформируте
        //значения "пустого" set, конвертируя строки в нижний регистр
        list<string> lString1 = {"qw", "RT", "Jh", "yT", "WWW", "rOOt"};
        print_info(lString1, "list lString1 created");

        set<string> setString1;
        print_info(setString1, "set setString1 created");
        transform(lString1.begin(), lString1.end(), insert_iterator<set<string>>(setString1, setString1.begin()), transform_to_lower());
        print_info(setString1, "set setString1 from list lString1");
    }

    {//copy_if
        //Дан вектор с элементами типа string. С помощью copy_if() требуется
        //вывести сначала строки, начинающиеся с буквы "А" или "а", затем с "Б"...
        //При этом порядок строк в исходном векторе менять не нужно!
        vector<string> vString1 = {"ad", "qw", "Bs", "Ae4t", "Cets", "RT", "Jh", "yT", "WWW", "rOOt"};
        print_info(vString1, "vector vString1 created");
        map<char,set<string>> msCopy;
        set<char, less<char>> setLetter;
        transform(vString1.begin(), vString1.end(),
                insert_iterator<set<char, less<char>>>(setLetter, setLetter.begin()),
                        [](string s) { return std::tolower(s[0]); });

        print_info(setLetter, "setLetter");

        class copy_if_letter {
        private:
            char letter;
        public:
            copy_if_letter(char c):letter(c) {};

            bool operator() (string s)
            {
                return (std::tolower(s[0]) == letter);
            }
        };

        for(set<char>::const_iterator iter = setLetter.begin(); iter != setLetter.end(); ++iter)
        {
            cout << "\n" << *iter << ": ";
            copy_if(vString1.begin(), vString1.end(), ostream_iterator<string>(cout, " # "), copy_if_letter(*iter));
        }
        cout << "\n";

        //Дан multimap, содержаций пары: "месяц - количество денй в месяце"
        //pair<string, int>. С помощью copy_if сформируйте ДВА map-а: первый -
        //с парами, содержащими четное количество дней, 2-ой - нечетное.
        std::multimap<string, int> month {
                {"January", 31}, {"February", 28}, {"February", 29}, { "March", 31},
                {"April", 30}, {"May",31}, {"June", 30}, {"July", 31}, {"August",31},
                {"September",30}, {"October", 31}, {"November",30}, {"December",31}
        };

       cout << "\nMap month ALL" << endl;
       for_each(month.begin(), month.end(), print_for_each_value<pair<string,int>>);

       class copy_if_even_odd {
       private:
           bool m_is_even;
       public:
           copy_if_even_odd(bool is_even) : m_is_even(is_even) {};

           bool operator()(const std::pair<string, int> &value) const
           {
                if ((value.second)%2 == 0)
                    return (m_is_even);
                else
                    return (!m_is_even);
           }
        };

       map<string,int> map_even, map_odd;
       copy_if(month.begin(), month.end(),
               insert_iterator<map<string,int>>(map_even, map_even.begin()), copy_if_even_odd(true));
       copy_if(month.begin(), month.end(),
                insert_iterator<map<string,int>>(map_odd, map_odd.begin()), copy_if_even_odd(false));

       //Распечатайте multimap и map-ы, используя написанный вами ранее шаблон
       //функции, выводящей элементы ЛЮБОГО контейнера на печать.
       //Что нужно сделать дополнительно для вывода пары?

       cout << "\nMap month EVEN" << endl;
       for_each(map_even.begin(), map_even.end(), print_for_each_value<pair<string,int>>);
       cout << "\nMap month ODD" << endl;
       for_each(map_odd.begin(), map_odd.end(), print_for_each_value<pair<string,int>>);
    }
}

int main()
{
    test_iterators1();
    test_algo2();
    test_algo3();

    return 0;
}
