#include "lab3_c220.h"

#include <string>
#include <iostream>
#include <cstdint>
#include <climits>          // for CHAR_BIT in gcc
#include <algorithm>
#include <iterator>
#include <memory>

#include <vector>
#include <list>
#include <deque>
#include <set>

#include <stack>
#include <queue>
#include <cstdint>

using namespace std;

void test1()
{
    cout << "\n\n*** Задание 1 ***\n\n";
    //Задание 1. Сырые строковые литералы (Raw String Literals)
    //Выведите строку, например: my name is "Marina"
    //а) без использования Raw String Literals
    //б) посредством Raw String Literals
    //в) добавьте разделители (Delimeter)

    {
        const char* s1 = "My name is \"Karina\"";
        const char* s2 = R"(My name is "Karina")";
        const char* s3 = R"aaa(My name is "Karina")aaa";
        cout << "a) " << s1 << endl;
        cout << "b) " << s2 << endl;
        cout << "c) " << s3 << endl;
    }
}
//-------------------------------------------------------------------------------
constexpr int factorial(int n)
{
    return (n == 0) ? 1 : n*factorial(n-1);
}

void test2()
{
    cout << "\n\n*** Задание 2 ***\n\n";
    //////////////////////////////////////////////////////////////////////////////////////////////
    //Задание 2. Реализуйте вычисление факториала с помощью constexpr-функции.
    //
    //Подсказки/напоминания:
    //		- constexpr – функция должна состоять из единственной инструкции return <выражение>; (пока!)
    //		- но это выражение может включать вызов другой constexpr – функции, в частности рекурсивный
    //		  вызов
    //		- если параметр рекурсивной constexpr- функции - это константа, компилятор вычислит результат
    //		  на этапе компиляции

    //Проверьте тот факт, что компилятор вычисляет значение на этапе компиляции (если в качестве
    //				параметра используется константа, известная компилятору на этапе компиляции).
    //				Для проверки достаточно создать встроенный массив с размерностью, вычисляемой
    //				посредством constexpr-функции:

    {	//Например:
        int ar[factorial(3)];
        cout << "size ar = " << size(ar) << endl;
        //или
        constexpr int n = factorial(5);
        int ar1[n];
        cout << "n = factorial(5): n = " << n << endl;

        //попробуйте:
        //int m = 7;
        const int m = 7;
        constexpr int n1 = factorial(m);
        cout << "n1 = factorial(" << m << "): n1 = " << n1 << endl;
        int ar2[n1];

        //а так?
        int n2 = factorial(m);
        cout << "n2 = factorial(" << m << "): n2 = " << n2 << endl;
    }
}
//-------------------------------------------------------------------------------
/*
constexpr unsigned int to_str(const char *s, unsigned int v)
    unsigned int t = 0;
    while  (*s != '\0')
    {
        t += (*s == '1') ? 1 : 0;

        s++;
        if (*s == '\0')
            break;

        t = t << 1;
    }

    return t;
}*/
constexpr unsigned int to_str(const char *s, unsigned int v)
{
    return (*s != '\0') ? to_str(s + 1, (v << 1 | (*s - '0'))) : v;
}
constexpr unsigned int operator""_b(const char *s)
{
  return to_str(s, 0);
}

string operator""_toBinStr(unsigned long long v)
{
    string s = "0b";
    for(unsigned long long i = CHAR_BIT*sizeof(v); i > 0; i--)
    {
        unsigned long long mask = 1LL << i-1LL;
        if (mask <= v)
        {
             s += ((mask & v) >= 1) ? '1' : '0';
        }
    }

    return s;
}

void test3()
{
    cout << "\n\n*** Задание 3 ***\n\n";
    //////////////////////////////////////////////////////////////////////////////////////////////
    //Задание 3a. Перевод с помощью пользовательского литерала из двоичного представления строкового
    //в значение, например: строку "100000000" -> в значение 256
    //Проверить результат посредством префикса 0b
    //Чтобы вызов пользовательского литерала выглядел просто и читаемо, например: 100000000_b
    //логично использовать пользовательский литерал с единственным параметром - const char*

    //Так как речь идет о литералах, логично вычислять значения на этапе компиляции
    // => реализуйте пользовательский литерал посредством constexpr - функций
    //Подсказка/напоминание:
    //		- constexpr – функция должна состоять из единственной инструкции return <выражение>;
    //		- но это выражение может включать вызов другой constexpr – функции,
    //		- которая может быть рекурсивной (если параметр такой функции - это константа,
    //		  компилятор вычислит результат вызова рекурсивной функции на этапе компиляции)

    {
        const int value_b = 100000000_b;
        int ar1[value_b] = {9};
        cout << "value_b: " << value_b << endl;

    }

    //Задание 3b. Перевод в строковое двоичное представление, например: 256 -> "0b100000000"
    //Так как строка может быть любой длины, логичнее и проще возвращать объект std::string
    //=> возвращаемое значение не может быть constexpr!
    //Подсказка: манипулятора std::bin пока нет => преобразование в двоичную строку
    //придется делать вручную
    //Подсказка: количество разрядов в байте определяет константа CHAR_BIT - <cstdint>

    {
        std::string sBin = 256_toBinStr;
        cout << "sBin: " << sBin << endl;
    }
}
//-------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////
//Задание 4а. constexpr - объекты
//Создать класс (шаблон класса?) для хранения и манипулирования диапазоном значений.
//В классе должны быть:
//	переменные для хранения минимального и максимального значений,
//	методы для получения каждого из значений
//	метод для проверки - попадает ли указанное значение в диапазон
//	метод, который получает любое значение данного типа и формирует результирующее значение:
//							если принадлежит диапазону, то его и возвращаем
//							если меньше минимального значения, возвращаем минимальное
//							если больше максимального значения, возвращаем максимальное

template <typename T> class MyRange
{
private:
    const pair<T,T> m_range;

public:
    constexpr MyRange() = default;
    constexpr MyRange(pair<T,T> range) : m_range(range) { };

    constexpr T getMin() const noexcept { return m_range.first; }
    constexpr T getMax() const noexcept { return m_range.second; }

    constexpr bool isRange(const T& value) const noexcept
    {
        return (value >= m_range.first && value <= m_range.second);
    }

    constexpr T getNormalizedValue(const T& value) const noexcept
    {
        if(isRange(value))
            return value;
        else
            return (value > m_range.second) ? m_range.second : m_range.first;
    }
};

void test4()
{
    cout << "\n\n*** Задание 4 ***\n\n";
    //Проверьте тот факт, что компилятор вычисляет значение на этапе компиляции.
    MyRange<int> mint1(pair<int,int>(1, 10));
    int ar1[mint1.getMin()];            // works on Linux gcc-9.3.1, gcc10, but MSVS 19 error
    int ar11[mint1.getMin()] = { 6 };   // works on Linux gcc-9.3.1, gcc10, but MSVS 19 error

    cout << "\nar11: ";
    for (auto i:ar11)
        cout << i << " ";

    constexpr int max = MyRange<int>(pair<int,int>(6, 60)).getMax();
    cout << "\nmax: " << max;

    constexpr MyRange<int> mint2(pair<int,int>(1, 9));
    int ar2[mint2.getMin()] = {5};

    cout << "\nar2: ";
    for (auto i:ar2)
        cout << i << " ";

    constexpr int n1 = MyRange<int>(pair<int,int>(6, 60)).getNormalizedValue(7);
    int m = 0;
    int n2 = MyRange<int>(pair<int,int>(6, 60)).getNormalizedValue(m);
}
//-------------------------------------------------------------------------------
//Задание 5.
/*
Реализуйте шаблон функции для печати любых последовательностей
 (vector, list, deque, set и встроенного массива), которые могут содержать:
•	как объекты любого типа,
•	так и указатели на объекты любого типа
 (указатели распечатывать неинтересно => в этом случае следует получать значение по адресу)
Подсказки: if constexpr
*/

template <typename T> void print_values(const T& value, const char* name)
{
    cout << name << ": ";

        for (const auto& v:value)
        {
            if constexpr (is_pointer_v<decay_t<decltype(v)>>)
             cout << " " << *v;
            else
             cout << " " << v;
        }

    cout << endl;
}


template <typename C> void free_cont(C& value)
{
    if constexpr (is_pointer_v<typename C::value_type>)
    {
        for_each(begin(value), end(value), [](typename C::value_type obj)
        {  delete obj; });
    }

    value.clear();
}

void test5()
{
    cout << "\n\n*** Задание 5 ***\n\n";
    vector<string> v1 = { "a", "d", "c" };
    vector<string *> p_v1 = { new string("a"), new string("d"), new string("c") };

    print_values(v1, "v1");
    print_values(p_v1, "p_v1");

    list<string> l1 = { "f", "g", "h" };
    list<string *> p_l1 = { new string("f"), new string("g"), new string("h")};

    print_values(l1, "l1");
    print_values(p_l1, "p_l1");

    free_cont(p_l1);

    int ar_i[] = { 6, 8, 9, 10, 11};
    double *ar_d[] = { new double(10.7), new double(4.6), new double(7.8)};

    print_values(ar_i, "ar_i");
    print_values(ar_d, "ar_d");

    for_each(begin(ar_d), end(ar_d), [](double * obj){ delete obj; });

    deque<float> deque_f1 = { 7.8, 9.0, 2.8 };
    deque<float *> p_deque_f1 = { new float(7.8), new float(9.0), new float(2.8) };

    print_values(deque_f1, "deque_f1");
    print_values(p_deque_f1, "p_deque_f1");

    free_cont(p_deque_f1);

    set<float> set_f1 = { 7.8, 9.0, 2.8 };
    set<float *> p_set_f1 = { new float(7.8), new float(9.0), new float(2.8) };

    print_values(set_f1, "set_f1");
    print_values(p_set_f1, "p_set_f1");

    free_cont(p_set_f1);
}
//-------------------------------------------------------------------------------

/***************************************************************/
//Задание 6.
/* Реализуйте шаблон функции сложения двух значений.
Если первое слагаемое является вектором, то все элементы вектора нужно увеличить на значение второго параметра.
При этом элементы вектора и второй параметр должны быть одного и того же типа.
Подсказки: if constexpr, is_same
*/
template <typename T, typename C> auto sum(T& value1, const C& value2)
{
    if constexpr (is_same_v<vector<decay_t<C>>, decay_t<T>>)
    {
        cout << "is vector and type is same\n";

        for (auto &v1:value1)
        {
            v1 += value2;
        }

        return value1;
    } else
    {
        decltype(value1+value2) sum = value1+value2;
        return sum;
    }
}

void test6()
{
    cout << "\n\n*** Задание 6 ***\n\n";
    vector<float> v_f1 = { 7.8, 9.0, 2.8 };
    print_values(v_f1, "v_f1");
    constexpr float add = 10;
    auto v_f2 = sum(v_f1, add);
    print_values(v_f2, "v_f2 = v_f1 added(10)");

    int s1 = 10, s2 = 8;
    auto f3 = sum(s1, s2);
    cout << "\nf3: " << f3;

}
//-------------------------------------------------------------------------------
//Задание 7.
/* 	Реализуйте шаблон функции вывода на печать значений элементов
любого адаптера (stack, queue, priority_queue)
Подсказки: if constexpr, is_same
Предусмотрите вывод значений, если в адаптере хранятся указатели.
*/
// дописать шаблон для получениия top или front элемент
template <typename Cont> decltype(auto) get_top(const Cont& value)
{
    if constexpr (is_same_v<queue<typename Cont::value_type, typename Cont::container_type>, Cont>)
        if constexpr (is_pointer_v<typename Cont::value_type>)
            return *(value.front());
        else
            return value.front();
    else
    if constexpr (is_pointer_v<typename Cont::value_type>)
       return *(value.top());
    else
       return value.top();
}

template<typename Cont> void print_queue(Cont value, const char* name)
{
    cout << name << ": ";

    while (!value.empty()) {
        cout << " " << get_top(value);
        value.pop();
    }

    cout << endl;
}

template<typename Cont> void free_queue(Cont& value)
{
    while (!value.empty()) {

        if constexpr (is_same_v<queue<typename Cont::value_type, typename Cont::container_type>, Cont>)
            if constexpr (is_pointer_v<typename Cont::value_type>)
            {
                auto tmp = value.front();
                delete tmp;
            }
        else
            {
                if constexpr (is_pointer_v<typename Cont::value_type>)
                {
                    auto tmp = value.top();
                    delete tmp;
                }
            }

        value.pop();
    }

    cout << endl;
}

void test7()
{
    cout << "\n\n*** Задание 7 ***\n\n";
    stack<string> s1({ "a", "d", "c" });
    stack<string*> p_s1({ new string("a"), new string("d"), new string("c") });

    print_queue(s1, "s1");
    print_queue(p_s1, "p_s1");

    free_queue(p_s1);

    queue<float> q1({ 7.8, 9.0, 2.8 });
    queue<float*> p_q1({ new float(7.8), new float(9.0), new float(2.8) });

    print_queue(q1, "q1");
    print_queue(p_q1, "p_q1");

    free_queue(p_q1);

    priority_queue<int> pque_f1({}, { 12, 65, 32 });
    priority_queue<int*> p_pque_f1({}, { new int(12), new int(65), new int(32) });

    print_queue(pque_f1, "pque_f1");
    print_queue(p_pque_f1, "p_pque_f1");

    free_queue(p_pque_f1);
}
//-------------------------------------------------------------------------------
template <typename T> constexpr T Smth()
{
    if constexpr (is_same_v<T, int>)
        return 1;
    else if constexpr (is_same_v<T, double>)
        return 2.2;
    else if constexpr (is_same_v<T, string>)
        return "abc";
    else
        return T{};
}

void test8()
{
//Задание 8.
/* 	Реализуйте шаблон constexpr функции Smth(),
которая должна возвращать значения разного типа
Подсказки: constexpr, if constexpr
*/
    cout << "\n\n*** Задание 8 ***\n\n";
    constexpr int res1 = Smth<int>();/*<вызов Smth()>;*/ //res1 = 1
    cout << "res1 = " << res1 << endl;
    constexpr double res2 = Smth<double>();/*<вызов Smth()>; */ //res2 = 2.2
    cout << "res2 = " << res2 << endl;
    /*constexpr??? - not work*/ std::string res3 = Smth<string>();/*<вызов Smth()>; */ //res3 = "abc"
    cout << "res3 = " << res3 << endl;
    constexpr float res4 = Smth<float>();
    cout << "res4 = " << res4 << endl;

}
//-------------------------------------------------------------------------------
//Задание 9.

/*Пользовательский deduction guide – для вывода типов параметров шаблона
Задан шаблон класса, который инкапсулирует внедренный ограниченный массив известной
размерности с элементами любого типа. */

template<typename T, size_t size> class MyArray
{
       T ar[size]{}; //как обеспечить инициализацию элементов базового типа по умолчанию нулем?

public:
       MyArray() = default;
       ~MyArray() = default;

       MyArray(const T* array_begin)
       {
           for (std::size_t i = 0; i < size; ++i)
           {
               ar[i] = array_begin[i];
           }
       }

    friend std::ostream& operator<<(std::ostream& os, const MyArray& my_array) {
        os << "{ ";
        for(const auto& a:my_array.ar)
        {
            os << a << " ";
        }
        os << "}" << std::endl;

        return os;
    }
};

template<typename T, size_t size> MyArray(const T (&arr)[size]) -> MyArray<T, size>;
void test9()
{
    cout << "\n\n*** Задание 9 ***\n\n";
    //Требуется обеспечить работоспособность приведенных примеров использования.
    MyArray<int, 5> ar1;//MyArray<int,5>
    cout << "MyArray<int, 5> ar1: " << ar1;
    MyArray ar2{"ABC"}; //MyArray<char,4>
    cout << "MyArray ar2{\"ABC\"}: " << ar2;
    int ar[] = { 1,2,3 };
    MyArray ar3{ ar };
    cout << "MyArray ar3{ ar = { 1,2,3 } }: " << ar3;
}
//-------------------------------------------------------------------------------
int main()
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
}
